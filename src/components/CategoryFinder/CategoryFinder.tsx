import {
  debounce,
  FormControl,
  FormGroup,
  Typography,
} from "@material-ui/core";
import React, { ChangeEvent, useEffect, useState } from "react";
import { OutlinedTextField } from "../../styledComponents/OutlinedTextField";
import CommissionPreview from "./components/CommissionPreview";
import {
  ICategory,
  ICategoryPreViewBody,
  ICategoryPreViewBodyDisplay,
} from "./Models";
import { groupsApi } from "../../api/groupsApi";
import { useSnackbar } from "notistack";
import { applicationsApi } from "../../api/votingsApi";

const CategoryFinder = ({
  getter,
  isOne = false,
  defaultSelectedCommissions,
  isRatePick = false,
}: {
  getter: (data: ICategoryPreViewBody[]) => void;
  isOne?: boolean;
  defaultSelectedCommissions?: ICategoryPreViewBody[];
  isRatePick?: boolean;
}) => {
  const [isLoading, setIsLoading] = useState(false);
  const [searchedCommissions, setSearchedCommissions] = useState<
    ICategoryPreViewBody[]
  >([]);
  const [selectedCommissions, setSelectedCommissions] = useState<
    ICategoryPreViewBody[]
  >([]);
  const [commissionsListDisplay, setCommissionsListDisplay] = useState<
    ICategoryPreViewBodyDisplay[]
  >([]);
  const [commissionFinder, setCommissionFinder] = useState<string>("");

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const fetchGroupsList = React.useCallback(
    debounce(async (payload: string) => {
      setSearchedCommissions((await fetchGrops(payload)) || []);
    }, 500),
    []
  );

  const { enqueueSnackbar } = useSnackbar();

  const fetchGrops = async (
    payload: string
  ): Promise<ICategory[] | undefined> => {
    try {
      setIsLoading(true);
      const resp = await applicationsApi.searchCategories(payload);
      if (resp) {
        return resp;
      }
    } catch (e: any) {
      console.log(e);
      enqueueSnackbar("Ошибка поиска", {
        variant: "error",
      });
    } finally {
      setIsLoading(false);
    }
  };

  const changeFindCommissionInputHandler = (
    event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setCommissionFinder(event.target.value);
    fetchGroupsList(event.target.value);
  };
  const selectCommissionHandler = (
    commission: ICategoryPreViewBody,
    rate?: string
  ) => {
    if (isOne) {
      selectedCommissions.map((x) => x.id).indexOf(commission.id) === -1
        ? setSelectedCommissions([commission])
        : setSelectedCommissions(
            selectedCommissions.slice().filter((x) => x.id !== commission.id)
          );
    } else {
      selectedCommissions.map((x) => x.id).indexOf(commission.id) === -1
        ? setSelectedCommissions([
            ...selectedCommissions,
            isRatePick ? { ...commission, categoryRate: rate } : commission,
          ])
        : setSelectedCommissions(
            selectedCommissions.slice().filter((x) => x.id !== commission.id)
          );
    }
  };
  useEffect(() => {
    defaultSelectedCommissions?.length &&
      setSelectedCommissions(defaultSelectedCommissions);
    fetchGroupsList("");
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    getter(selectedCommissions);
  }, [getter, selectedCommissions]);
  useEffect(() => {
    setCommissionsListDisplay(
      [
        ...selectedCommissions,
        ...searchedCommissions
          .slice()
          .filter((x) => !selectedCommissions.map((y) => y.id).includes(x.id)),
      ].map((x) => {
        return {
          ...x,
          isSelected: selectedCommissions.indexOf(x) !== -1,
        };
      }) as ICategoryPreViewBodyDisplay[]
    );
  }, [searchedCommissions, selectedCommissions]);
  return (
    <div style={{ width: "100%" }}>
      <FormControl component="fieldset" fullWidth style={{ marginTop: 10 }}>
        <FormGroup aria-label="position" row>
          <OutlinedTextField
            // className={classes.newVoteField}
            label="Категории"
            InputLabelProps={{
              shrink: true,
            }}
            autoComplete="off"
            value={commissionFinder}
            variant="outlined"
            type="text"
            name="commission"
            fullWidth
            onChange={changeFindCommissionInputHandler}
          />
          <CommissionPreview
            {...{
              data: commissionsListDisplay,
              handler: selectCommissionHandler,
              isLoading,
              isRatePick,
            }}
          />
        </FormGroup>
      </FormControl>
    </div>
  );
};

export default CategoryFinder;
