import { IUser } from "../AccountInfo/Models";

export interface ICategory {
  name: string;
  id: string;
  categoryRate?: string;
}

export interface ICategoryPreViewBody extends ICategory {
  handler?: CategoryHandlerType;
}
export type CategoryHandlerType = (
  commission: ICategoryPreViewBody,
  rate?: string
) => void;
export interface ICategoryPreViewBodyDisplay extends ICategoryPreViewBody {
  isSelected: boolean;
  isRatePick: boolean;
}
