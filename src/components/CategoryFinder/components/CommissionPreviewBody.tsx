import { Box, Button, TextField, Tooltip, Typography } from "@material-ui/core";
import { useSnackbar } from "notistack";
import React from "react";
import { isMobile } from "../../../const/Size";
import { palette } from "../../../const/style/palette";
import { ICategoryPreViewBodyDisplay } from "../Models";

const CommissionPreviewBody = ({
  handler = () => {},
  isSelected,
  isRatePick,
  ...oneCommission
}: ICategoryPreViewBodyDisplay) => {
  const { enqueueSnackbar } = useSnackbar();
  console.log(oneCommission);
  const [rate, setRate] = React.useState(
    oneCommission.categoryRate === undefined ||
      oneCommission.categoryRate === null
      ? ""
      : oneCommission.categoryRate.toString()
  );
  // return <div/>;
  return (
    <div
      style={{
        justifyContent: "space-between",
        display: isMobile ? "block" : "flex",
        padding: 10,
        backgroundImage: isSelected ? palette.primaryGradient : undefined,
        borderRadius: 3,
        margin: 6,
        borderBottom: "1px solid #ccc",
      }}
      onClick={() => {
        (!isRatePick || isSelected) && handler(oneCommission, rate);
      }}
    >
      <div style={{ textAlign: "left" }}>
        <Typography
          variant="subtitle1"
          color={!isSelected ? "initial" : "textPrimary"}
        >
          {console.log(rate)}
          {rate
            ? `${oneCommission.name.slice(0, 60)} - ${rate}%`
            : oneCommission.name.slice(0, 60)}
        </Typography>
        {isRatePick && !isSelected && (
          <>
            <Tooltip
              title="Введите процент, который будет получать фонд от заявки данной категории"
              enterDelay={800}
            >
              <TextField
                value={rate}
                onChange={(event: any) => {
                  setRate(event.target.value);
                }}
                label="Ставка фонда"
                fullWidth
                type="number"
              />
            </Tooltip>
          </>
        )}
      </div>
      <Box textAlign="right">
        <Typography
          variant={isMobile ? "subtitle1" : "inherit"}
          style={{ textAlign: "right" }}
          color={!isSelected ? "initial" : "textPrimary"}
        >
          id: #{oneCommission.id}
        </Typography>
        {isRatePick && !isSelected && (
          <Button
            variant="outlined"
            fullWidth
            color="primary"
            onClick={() => {
              if (!isNaN(parseFloat(rate))) {
                handler(oneCommission, rate);
              } else {
                enqueueSnackbar("Ставка по категории должна быть числом", {
                  variant: "error",
                });
              }
            }}
            style={{
              marginTop: 10,
            }}
          >
            Выбрать
          </Button>
        )}
      </Box>
    </div>
  );
};

export default CommissionPreviewBody;
