import { LinearProgress, makeStyles, Paper } from "@material-ui/core";
import CommissionPreviewBody from "./CommissionPreviewBody";
import { CategoryHandlerType, ICategoryPreViewBodyDisplay } from "../Models";

const stylesCommissionPreview = makeStyles(() => ({
  wrapper: {
    backgroundColor: "#fff",
    marginTop: 20,
    marginBottom: 20,
    width: "100%",
    zIndex: 2,
  },
}));

const CommissionPreview = ({
  data,
  handler,
  isLoading,
  isRatePick,
}: {
  data: ICategoryPreViewBodyDisplay[];
  handler: CategoryHandlerType; // Тут плохо.
  isLoading: boolean;
  isRatePick: boolean;
}) => {
  const classes = stylesCommissionPreview();

  return (
    <Paper className={classes.wrapper} elevation={0}>
      {isLoading && <LinearProgress />}
      {data.slice(0, 100).map((commissionData) => (
        <CommissionPreviewBody
          {...commissionData}
          handler={handler}
          key={commissionData.id}
          isRatePick={isRatePick}
        />
      ))}
    </Paper>
  );
};

export default CommissionPreview;
