import { Container } from "@material-ui/core";
import React, { useContext } from "react";
import { Route, Switch } from "react-router";
import { RedirectLinks } from "../../const/RedirectLinks";
import BeneficiaryReview from "../BeneficiaryReview/BeneficiaryReview";
import BlockchainStatistic from "../BlockchainStatistic/BlockchainStatistic";
import CommissionsReview from "../CommissionsReview/CommissionsReview";
import ErrorScreen from "../ErrorScreen/ErrorScreen";
import FullVote from "../FullVote/FullVote";
import LoginScreen from "../LoginScreen/LoginScreen";
import { ELoadingState } from "../LoginScreen/Models";
import { UserContext } from "../LoginScreen/UserStore";
import MyVotesList from "../MyVotesList/MyVotesList";
import StartPage from "../StartPage/StartPage";
import VotesParticipateList from "../VotesParticipateList/VotesParticipateList";
import VotesReview from "../VotesReview/VotesReview";

function PageContent() {
  const { state } = useContext(UserContext);

  return (
    <div
      style={{
        paddingTop: 20,
        flexGrow: 1,
        paddingBottom: 20,
      }}
    >
      <Container maxWidth="lg">
        <main>
          {(state.LodingState === ELoadingState.ERROR ||
            state.LodingState === ELoadingState.NEVER) && (
            <Switch>
              <Route
                path={RedirectLinks.LOGIN_SCREEN}
                exact
                component={LoginScreen}
              />
               <Route
                path={RedirectLinks.BLOCKCHAIN_STATISTIC}
                exact
                component={BlockchainStatistic}
              />
              <Route path="/" exact component={StartPage} />
              <Route path="*" component={ErrorScreen} />
            </Switch>
          )}
          {state.LodingState === ELoadingState.LOADED && (
            <Switch>
               <Route
                path={RedirectLinks.BLOCKCHAIN_STATISTIC}
                exact
                component={BlockchainStatistic}
              />
              <Route
                path={RedirectLinks.VOTES_PARTICIPATE_LIST}
                exact
                component={VotesParticipateList}
              />
              <Route
                path={RedirectLinks.MY_VOTES_LIST}
                exact
                component={MyVotesList}
              />
              <Route
                path={RedirectLinks.APPLICATIONS_REVIEW}
                exact
                component={VotesReview}
              />
              <Route
                path={RedirectLinks.BENEFICIARY_REVIEW}
                exact
                component={BeneficiaryReview}
              />
              <Route
                path={RedirectLinks.FUND_REVIEW}
                exact
                component={CommissionsReview}
              />

              <Route
                path={RedirectLinks.ONE_VOTE + ":id"}
                exact
                component={FullVote}
              />
              <Route
                path={RedirectLinks.ADMIN_ONE_VOTE + ":id"}
                exact
                component={() => <FullVote isAdmin={true} />}
              />
              <Route path="/" exact component={StartPage} />
              <Route path="*" component={ErrorScreen} />
            </Switch>
          )}
        </main>
      </Container>
    </div>
  );
}

export default PageContent;
