import { LinearProgress, Typography } from "@material-ui/core";
import { useSnackbar } from "notistack";
import React, { useContext } from "react";
import { useParams } from "react-router";
import { applicationsApi } from "../../api/votingsApi";
import { UserContext } from "../LoginScreen/UserStore";
import { IVoteBlockProps, VoteStatus } from "../VoteBlock/state";
import VoteBlock from "../VoteBlock/VoteBlock";

interface IParams {
  id: string;
}
interface IProps {
  isAdmin?: boolean;
}
const FullVote = ({ isAdmin }: IProps) => {
  const { state } = useContext(UserContext);

  const [isLoading, setIsLoading] = React.useState(false);
  const id = useParams<IParams>().id;
  const { enqueueSnackbar } = useSnackbar();
  const [fullVote, setFullVote] = React.useState<IVoteBlockProps>();
  React.useEffect(() => {
    (async () => {
      setIsLoading(true);
      try {
        const vote: IVoteBlockProps = await applicationsApi.fetchVotingById(
          id,
          state.data?.userData?.sub
        );
        const chat = await applicationsApi.chatFetch(id);
        if (vote) {
          setFullVote({ ...vote, isFull: true, chat });
        }
      } catch (e: any) {
        enqueueSnackbar("Ошибка загрузки голосования", {
          variant: "error",
        });
      } finally {
        setIsLoading(false);
      }
    })();
  }, [enqueueSnackbar, id, state.data?.userData?.sub]);
  if (isLoading) return <LinearProgress />;
  if (fullVote)
    return (
      <VoteBlock {...fullVote} key={fullVote.id} isToAdminRoute={isAdmin} />
    );

  return <Typography>Данного голосования не существует</Typography>;
};

export default FullVote;
