import { Paper, Typography } from "@material-ui/core";
import React from "react";

const Headline = ({ text }: { text: string }) => {
  return (
    <Paper style={{ marginTop: 10, marginBottom: 10, textAlign: "left" }}>
      <Typography variant="h5" style={{ padding: 18 }}>
        {text}
      </Typography>
    </Paper>
  );
};

export default Headline;
