import { Box, Grid, LinearProgress, Typography } from "@material-ui/core";
import { useEffect, useState, useRef, useContext } from "react";
import { applicationsApi } from "../../api/votingsApi";
import { isMobile } from "../../const/Size";
import useFetchOnScroll from "../../hooks/useFetchOnScroll";
import { listSorts } from "../../tools/listSorts";
import Footer from "../Footer/Footer";
import { UserContext } from "../LoginScreen/UserStore";
import { IVoteBlockProps, VoteStatus } from "../VoteBlock/state";
import VoteBlock from "../VoteBlock/VoteBlock";
import VotesSort from "./components/VotesSort";

const VotesParticipateList = () => {
  const { state } = useContext(UserContext);
  const [order, setOrder] = useState(true);
  const [field, setField] = useState<string>();
  const [tab, setTab] = useState(0);
  const loaderRef = useRef<HTMLDivElement>(null);
  const [isVotesFinished, setIsVotesFinished] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);

  const [allVotes, setAllVotes] = useState<IVoteBlockProps[]>([]);
  const [displayableVotes, setDisplayableVotes] = useState<IVoteBlockProps[][]>(
    [[], []]
  );

  const fetchVotes = () => {
    (async () => {
      const fetchedVotes = await applicationsApi.fetchVotings(
        currentPage * 10,
        10,
        [],
        {
          categoryId: field,
          ascending: order,
          isCrmManager: false,
          isMyApplications: tab === 1,
        }
      );
      fetchedVotes?.length
        ? (() => {
            const otherVotings = fetchedVotes.filter(
              (vote) => true
              // vote.status === VoteStatus.VOTED
              // ||
              // vote.createdBy.id !== state.data?.userData?.sub
            );
            setCurrentPage(currentPage + 1);
            setAllVotes([...allVotes, ...otherVotings]);
          })()
        : (() => {
            resetObserver();
            setIsVotesFinished(true);
          })();
    })();
  };
  const [resetObserver] = useFetchOnScroll(loaderRef, fetchVotes);

  useEffect(() => {
    setDisplayableVotes(listSorts.sortToColumns(allVotes, (x) => 1));

    return resetObserver;
  }, [allVotes, resetObserver]);

  const sortFieldHandler = (field: string) => {
    setAllVotes([]);
    setCurrentPage(0);
    setField(field);
    setIsVotesFinished(false);
  };
  const tabHandler = (tab: number) => {
    setAllVotes([]);
    setCurrentPage(0);
    setTab(tab);
    setIsVotesFinished(false);
  };
  const orderHendler = (order: boolean) => {
    setAllVotes([]);
    setCurrentPage(0);
    setOrder(order);
    setIsVotesFinished(false);
  };

  // useEffect(() => {
  //   fetchVotes();
  // }, [order, field]);

  return (
    <>
      <VotesSort
        sortFieldHandler={sortFieldHandler}
        orderHendler={orderHendler}
        tabHendler={tabHandler}
      />
      <div>
        {isMobile ? (
          allVotes.map((vote) => (
            <Box marginBottom={3} key={vote.id}>
              <VoteBlock {...vote} key={vote.id} />
            </Box>
          ))
        ) : (
          <Grid container justify="center" spacing={3}>
            <Grid item xs={5}>
              {displayableVotes[0].map((vote) => (
                <Box marginBottom={3} key={vote.id}>
                  <VoteBlock {...vote} key={vote.id} />
                </Box>
              ))}
            </Grid>
            <Grid item xs={5}>
              {displayableVotes[1].map((vote, index) => (
                <Box marginBottom={3} key={vote.id}>
                  <VoteBlock {...vote} key={vote.id} />
                </Box>
              ))}
            </Grid>
          </Grid>
        )}
        {isVotesFinished ? (
          <>
            <Typography>Вы просмотрели все заявки</Typography>
            <Footer />
          </>
        ) : (
          <div ref={loaderRef}>
            <LinearProgress />
          </div>
        )}
      </div>
    </>
  );
};

export default VotesParticipateList;
