import { Select, MenuItem, Typography, Fab, Paper } from "@material-ui/core";
import React, { useState } from "react";
import { HeadSelect } from "../../../styledComponents/HeadSelect";
import ExpandLessIcon from "@material-ui/icons/ExpandLess";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import { applicationsApi } from "../../../api/votingsApi";

interface IProps {
  sortFieldHandler: (field: string) => void;
  orderHendler: (order: boolean) => void;
  tabHendler: (tab: number) => void;
}

const VotesSort = ({ sortFieldHandler, orderHendler, tabHendler }: IProps) => {
  const [allCategories, setAllCategories] = useState<{ id: any; name: any }[]>(
    []
  );
  const [tab, setTab] = useState(0);
  const [isIncrease, setIsIncrease] = useState<boolean>(true);
  const [sortType, setSortType] = useState<number>(-1);
  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    console.error(event.target.value)
    if (event.target.value === -1){
      sortFieldHandler(null as unknown as string);
    }
    else{
      sortFieldHandler(event.target.value as string);
    }
    setSortType(event.target.value as number);
  };
  React.useEffect(() => {
    (async () => {
      try {
        setAllCategories([
          { id: -1, name: "Все категории" },
          ...(await applicationsApi.getCategories()),
        ]);
      } catch {}
    })();
  }, []);
  const handleReverse = () => {
    orderHendler(!isIncrease);
    setIsIncrease(!isIncrease);
  };

  const tabChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    tabHendler(event.target.value as number);
    setTab(event.target.value as number);
  };

  return (
    <Paper
      elevation={1}
      style={{
        display: "flex",
        marginBottom: 24,
        paddingTop: 10,
        position: "relative",
        padding: 10,
      }}
    >
      <div
        style={{
          marginLeft: 10,
          marginRight: 10,
          position: "relative",
          width: 120,
        }}
      >
        <Typography
          variant="body2"
          style={{
            position: "absolute",
            top: "50%",
            transform: "translateY(-50%)",
          }}
        >
          Категория
        </Typography>
      </div>
      <Select
        value={sortType}
        IconComponent={() => <ArrowDropDownIcon style={{ color: "#222" }} />}
        onChange={handleChange}
        input={<HeadSelect />}
      >
        {allCategories.map((x: any) => (
          <MenuItem value={x.id} key={x.id}>
            {x.name}
          </MenuItem>
        ))}
      </Select>
      <Select
        value={tab}
        IconComponent={() => <ArrowDropDownIcon style={{ color: "#222" }} />}
        onChange={tabChange}
        input={<HeadSelect />}
      >
        <MenuItem key={0} value={0}>
          Нераспределенные заявки
        </MenuItem>
        <MenuItem key={1} value={1}>
          Выбранные мной заявки
        </MenuItem>
      </Select>
      {/* <div
        style={{
          position: "absolute",
          right: 0,
          top: "50%",
        }}
      >
        <div
          style={{
            position: "relative",
            width: 120,
            marginRight: 10,
          }}
        >
          <Fab
            onClick={handleReverse}
            size="small"
            variant="extended"
            color="default"
            disableRipple
            style={{
              position: "absolute",
              top: "50%",
              right: 10,
              transform: "translateY(-50%)",
              textAlign: "right",
              borderRadius: 2,
              backdropFilter: "blur(10px)",
              backgroundColor: "rgba(255,255,255,03)",
            }}
          >
            {isIncrease ? <ExpandLessIcon /> : <ExpandMoreIcon />}
          </Fab>
        </div>
      </div> */}
    </Paper>
  );
};

export default VotesSort;
