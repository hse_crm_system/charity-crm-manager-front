export enum MenuButtonEnum {
  VOTES_PARTICIPATE_LIST = "/VotesParticipateList/",
  ACCOUNT_INFO = "/accountInfo/",
  MY_VOTES_LIST = "/myVotes/",
  BENEFICIARY_REVIEW = "/beneficiaryReview/",
  FUND_REVIEW = "/commissionsReview/",
  APPLICATIONS_REVIEW = "/votesReview/",
  LOGOUT = "LOGOUT",
}