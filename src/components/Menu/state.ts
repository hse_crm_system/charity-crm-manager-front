import { MenuButtonEnum } from "./Enums";

export interface IMenuProps {
  buttonsDataList: IButtonsData[];
}
export interface IButtonsData {
    text: string;
    link: string;
    handler?: any;
    type: MenuButtonEnum
}

