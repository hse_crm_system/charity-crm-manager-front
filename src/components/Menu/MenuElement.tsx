import { Box, Typography } from "@material-ui/core";
import React from "react";
import { useHistory, useLocation } from "react-router";
import { palette } from "../../const/style/palette";
import { MenuButton } from "../../styledComponents/buttons/MenuButton";
import GetStartIcon from "./GetStartIcon";
import {  IButtonsData } from "./state";

interface IProps {
  redirectHandler?: () => void;
  buttonData: IButtonsData;
}

const MenuElement = ({ redirectHandler, buttonData }: IProps) => {
  const path = useLocation().pathname;
  const history = useHistory();
  const redirectOn = (link: string) => {
    history.push(link);
    if (redirectHandler) {
      redirectHandler();
    }
  };
  return (
    <Box maxWidth={260}>
      <MenuButton
        startIcon={
          <GetStartIcon
            type={buttonData.type}
            style={{
              margin: 5,
              color:
                path === buttonData.link ? palette.contrastText : palette.text,
            }}
          />
        }
        style={{
          marginTop: 5,
          backgroundImage:
            path === buttonData.link ? palette.primaryGradient : undefined,
          justifyContent: "flex-start",
          textAlign: "left",
        }}
        onClick={() => {
          if (typeof buttonData.handler === "function") {
            buttonData.handler();
          }
          redirectOn(buttonData.link);
        }}
        variant="text"
        disableRipple
        fullWidth
      >
        <Typography
          variant="subtitle1"
          style={{
            color:
              path === buttonData.link ? palette.contrastText : palette.text,
          }}
        >
          {buttonData.text}
        </Typography>
      </MenuButton>
    </Box>
  );
};

export default MenuElement;
