import React from "react";
import AllInboxIcon from "@material-ui/icons/AllInbox";
import ClassIcon from "@material-ui/icons/Class";
import { MenuButtonEnum } from "./Enums";
import MarkunreadMailboxIcon from "@material-ui/icons/MarkunreadMailbox";
import ChromeReaderModeIcon from '@material-ui/icons/ChromeReaderMode';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';

interface IProps {
  type: MenuButtonEnum;
  style: any;
}
const GetStartIcon = (props: IProps) => {
  const { type, style } = props;
  switch (type) {
    case MenuButtonEnum.MY_VOTES_LIST:
      return <ClassIcon style={style} />;
    case MenuButtonEnum.ACCOUNT_INFO:
      return <ClassIcon style={style} />;
    case MenuButtonEnum.LOGOUT:
      return <ExitToAppIcon style={style} />;
    case MenuButtonEnum.FUND_REVIEW:
      return <MarkunreadMailboxIcon style={style} />;
      case MenuButtonEnum.APPLICATIONS_REVIEW:
        return <ChromeReaderModeIcon style={style} />;
    default:
      return <AllInboxIcon style={style} />;
  }
};

export default GetStartIcon;
