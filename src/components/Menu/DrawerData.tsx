import { Box } from "@material-ui/core";
import MenuElement from "./MenuElement";
import { IButtonsData, IMenuProps } from "./state";

const DrawerData = ({
  drawerProps,
  redirectHandler,
}: {
  drawerProps: IMenuProps;
  redirectHandler?: () => void;
}) => {
  return (
    <Box margin="20px" textAlign="left">
      {drawerProps.buttonsDataList.map((buttonData: IButtonsData, index) => (
        <MenuElement {...{ buttonData, redirectHandler }} key={index} />
      ))}
    </Box>
  );
};

export default DrawerData;
