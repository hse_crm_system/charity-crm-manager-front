import { Box, Drawer, Fab, Hidden } from "@material-ui/core";
import React, { useState } from "react";
import { IMenuProps } from "./state";
import DrawerData from "./DrawerData";
import MenuIcon from "@material-ui/icons/Menu";
import AccontInfo from "../AccountInfo/AccontInfo";

// ЛОгику выделения активной вкладки сделаем тут
const Menu = ({ buttonsDataList }: IMenuProps) => {
  const [mobileOpen, setMobileOpen] = useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  return (
    <>
      <Hidden lgUp implementation="css">
        <Fab
          style={{
            position: "fixed",
            zIndex: 10,
            bottom: 15,
            right: 15,
            borderRadius: 2,
          }}
          size="small"
          color="primary"
          aria-label="Menu"
          onClick={handleDrawerToggle}
        >
          <MenuIcon />
        </Fab>
        <Drawer
          PaperProps={{ style: { borderWidth: 0 } }}
          variant="persistent"
          anchor={"left"}
          open={mobileOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true,
          }}
        >
          <DrawerData
            {...{
              drawerProps: { buttonsDataList },
              redirectHandler: handleDrawerToggle,
            }}
          />
        </Drawer>
      </Hidden>
      <Hidden mdDown implementation="css">
        {/* <Drawer
          style={{
            flexShrink: 0,
          }}
          variant="persistent"
          open
          PaperProps={{ style: { borderWidth: 0 } }}
        > */}
        <Box position="sticky" top={25} marginTop={12} maxWidth={300}>
          <Box marginX={2} marginY={1}>
            <AccontInfo />
          </Box>
          <DrawerData
            {...{
              drawerProps: { buttonsDataList },
            }}
          />
        </Box>

        {/* </Drawer> */}
      </Hidden>
    </>
  );
};

export default Menu;
