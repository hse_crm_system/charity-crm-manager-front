import { Button, Typography } from "@material-ui/core";
import React, { useContext } from "react";
import { useHistory } from "react-router";
import { RedirectLinks } from "../../const/RedirectLinks";
import { UserContext } from "../LoginScreen/UserStore";

const StartPage = () => {
  const history = useHistory();
  const { state } = useContext(UserContext);
  const redirectToLogin = () => {
    history.push(RedirectLinks.LOGIN_SCREEN);
  };
  return (
    <div>
      <Typography>Start page</Typography>
      {!state.data?.userData && (
        <Button variant="contained" color="primary" onClick={redirectToLogin}>
          Start
        </Button>
      )}
    </div>
  );
};

export default StartPage;
