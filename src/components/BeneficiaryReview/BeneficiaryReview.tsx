import {
  TableContainer,
  Paper,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  TableFooter,
  TablePagination,
  IconButton,
  useTheme,
  ButtonGroup,
  Button,
  LinearProgress,
  Typography,
} from "@material-ui/core";
import { TablePaginationActionsProps } from "@material-ui/core/TablePagination/TablePaginationActions";
import { KeyboardArrowRight, KeyboardArrowLeft } from "@material-ui/icons";
import React from "react";
import FirstPageIcon from "@material-ui/icons/FirstPage";
import LastPageIcon from "@material-ui/icons/LastPage";
import { EBeneficiarStatuses, IBeneficiar } from "./models";
import { palette } from "../../const/style/palette";
import { userApi } from "../../api/userApi";
import CheckIcon from "@material-ui/icons/Check";
import ClearIcon from "@material-ui/icons/Clear";
import InfoIcon from "@material-ui/icons/Info";
import { useSnackbar } from "notistack";
const itemsPerPage = 20;

function TablePaginationActions(props: TablePaginationActionsProps) {
  const theme = useTheme();
  const { count, page, rowsPerPage, onChangePage } = props;

  const handleFirstPageButtonClick = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    onChangePage(event, 0);
  };

  const handleBackButtonClick = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    onChangePage(event, page - 1);
  };

  const handleNextButtonClick = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    onChangePage(event, page + 1);
  };

  const handleLastPageButtonClick = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <div style={{ display: "flex" }}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="previous page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowRight />
        ) : (
          <KeyboardArrowLeft />
        )}
      </IconButton>
      <IconButton disabled>
        <Typography>{page}</Typography>
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowLeft />
        ) : (
          <KeyboardArrowRight />
        )}
      </IconButton>
    </div>
  );
}

const BeneficiaryReview = () => {
  const [currentPage, setCurrentPage] = React.useState(0);
  const [data, setData] = React.useState<IBeneficiar[]>();
  const [isLoading, setIsLoading] = React.useState(false);
  const handleChangePage = async (_: any, newPage: number) => {
    setCurrentPage(newPage);
    fetchData(newPage);
  };
  const fetchData = async (page: number) => {
    try {
      setIsLoading(true);
      setData(
        await userApi.fetchbeneficiarApplications(
          page * itemsPerPage,
          itemsPerPage
        )
      );
    } catch {
    } finally {
      setIsLoading(false);
    }
  };
  const { enqueueSnackbar } = useSnackbar();

  React.useEffect(() => {
    fetchData(currentPage);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  const approveApplication = async (userId: string) => {
    try {
      const isSuccess = await userApi.approveBeneficiarApplication(userId);
      if (isSuccess) {
        enqueueSnackbar("Заявка подтверждена", {
          variant: "success",
        });
      } else {
        enqueueSnackbar("Ошибка подтверждения", {
          variant: "error",
        });
      }
    } catch {
    } finally {
      fetchData(currentPage);
    }
  };
  const declineApplication = async (userId: string) => {
    try {
      const isSuccess = await userApi.declineBeneficiarApplication(userId);
      if (isSuccess) {
        enqueueSnackbar("Заявка отклонена", {
          variant: "success",
        });
      } else {
        enqueueSnackbar("Ошибка отклонения", {
          variant: "error",
        });
      }
    } catch {
    } finally {
      fetchData(currentPage);
    }
  };

  return (
    <TableContainer component={Paper}>
      {isLoading && <LinearProgress />}
      <Table size="medium" aria-label="a dense table">
        <TableHead>
          <TableRow>
            <TableCell>Комментарий</TableCell>
            <TableCell align="right">Дополнительная информация</TableCell>
            <TableCell align="right">Id пользователя</TableCell>
            <TableCell align="right">Время подачи заявки</TableCell>
            <TableCell align="right">
              Время принятия решения по заявке
            </TableCell>
            <TableCell align="right">Статус</TableCell>
            <TableCell align="center">Действия</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {data &&
            data.map((row) => (
              <TableRow key={row.id}>
                <TableCell component="th" scope="row">
                  {row.comment}
                </TableCell>
                <TableCell align="right">{row?.additionalInfo}</TableCell>
                <TableCell align="right">{row.userId}</TableCell>
                <TableCell align="right">
                  {row?.creationTime &&
                    new Date(row?.creationTime).toLocaleString()}
                </TableCell>
                <TableCell align="right">
                  {row.approvalTime &&
                    new Date(row.approvalTime).toLocaleString()}
                </TableCell>
                <TableCell align="right">
                  {(() => {
                    switch (row.applicationStatus) {
                      case EBeneficiarStatuses.Approved:
                        return (
                          <CheckIcon
                            fontSize="small"
                            style={{
                              color: palette.acceptVote,
                            }}
                          />
                        );
                      case EBeneficiarStatuses.Created:
                        return (
                          <InfoIcon
                            fontSize="small"
                            style={{
                              color: palette.skipVote,
                            }}
                          />
                        );
                      case EBeneficiarStatuses.Declined:
                        return (
                          // <IconButton
                          //   disabled
                          //   size="small"
                          //   style={{
                          //     backgroundColor: palette.rejectVote,
                          //     color: "#FFF !important",
                          //   }}
                          // >
                          <ClearIcon
                            fontSize="small"
                            style={{
                              color: palette.rejectVote,
                            }}
                          />
                          // </IconButton>
                        );
                      default:
                        break;
                    }
                  })()}
                </TableCell>
                <TableCell align="center">
                  <ButtonGroup
                    //   className="vote-block-wrapper__button-group"
                    variant="contained"
                    aria-label="contained primary button group"
                    disableElevation
                    color="primary"
                    orientation="vertical"
                  >
                    {row.applicationStatus === EBeneficiarStatuses.Created && (
                      <>
                        <Button
                          style={{
                            backgroundColor: palette.acceptVote,
                            marginBottom: 10,
                          }}
                          onClick={() => {
                            approveApplication(row.userId);
                          }}
                        >
                          Подтвердить
                        </Button>
                        <Button
                          style={{ backgroundColor: palette.rejectVote }}
                          onClick={() => {
                            declineApplication(row.userId);
                          }}
                        >
                          Отказать
                        </Button>
                      </>
                    )}
                    {/* {row.applicationStatus === EBeneficiarStatuses.Approved && (
                      <>
                        <Button color="primary">Отозвать статус</Button>
                      </>
                    )} */}
                  </ButtonGroup>
                </TableCell>
              </TableRow>
            ))}
        </TableBody>
        <TableFooter>
          <TableRow>
            <TablePagination
              rowsPerPageOptions={[]}
              //   colSpan={3}
              count={NaN}
              rowsPerPage={itemsPerPage}
              page={currentPage}
              SelectProps={{
                inputProps: { "aria-label": "" },
                native: true,
              }}
              onChangePage={handleChangePage}
              ActionsComponent={TablePaginationActions}
            />
          </TableRow>
        </TableFooter>
      </Table>
    </TableContainer>
  );
};

export default BeneficiaryReview;
