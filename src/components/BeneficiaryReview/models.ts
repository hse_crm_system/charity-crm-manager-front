export interface IBeneficiar {
  id: number;
  userId: string;
  comment?: string;
  additionalInfo?: string;
  crmManagerId?: string;
  approvalTime?: string;
  applicationStatus: EBeneficiarStatuses;
  creationTime?: string;
  versionTime?: string;
}

export enum EBeneficiarStatuses {
  Created = "Created",
  Approved = "Approved",
  Declined = "Declined",
}
