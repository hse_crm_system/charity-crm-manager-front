export interface IUser {
  id: string;
  username: string;
  firstName: string;
  lastName: string;
}
export interface IUserPreview extends IUser {
  handler: UserHandlerType;
}
export interface IUserDisplay extends IUserPreview {
  isSelected: boolean;
}

export type UserHandlerType = (user: IUser) => void;
