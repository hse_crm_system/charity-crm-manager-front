import { Box, Paper, Typography } from "@material-ui/core";
import React, { useContext } from "react";
import { UserContext } from "../LoginScreen/UserStore";

const AccontInfo = () => {
  const { state } = useContext(UserContext);

  return (
    <Paper
      elevation={1}
      style={{
        display: "flex",
        marginBottom: 24,
        paddingTop: 10,
        position: "relative",
        padding: 10,
      }}
    >
      <Box marginX={2}>
      <Typography variant="h6" style={{}}>
        {state.data?.userData?.name || ""}
      </Typography>
      </Box>
    </Paper>
  );
};

export default AccontInfo;
