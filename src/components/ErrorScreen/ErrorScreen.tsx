import { Button, Grid, Typography } from '@material-ui/core';
import React from 'react';
import { useHistory } from 'react-router';

const ErrorScreen = () => {
    const history = useHistory();
    const redirectHome = () => {
        history.push("/");
    }
    return (
        <Grid container spacing={0} direction="column" alignItems="center" justify="center" style={{minHeight: '100vh'}}>
            <Grid item xs={3}>
                <Typography variant="h5">
                    Error 404
                </Typography>
                <Typography variant="h3">
                Page not found.
                </Typography>
                <Button onClick={redirectHome}color="primary">
                    Вернуться на главную страницу
                </Button>
            </Grid>
        </Grid>
    );
};

export default ErrorScreen;
