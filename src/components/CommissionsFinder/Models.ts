import { ICategoryPreViewBody } from "./../CategoryFinder/Models";
import { IUser } from "../AccountInfo/Models";

export interface ICommission {
  name: string;
  id: string;
  description?: string;
  participants?: IUser[];
  categories?: ICategoryPreViewBody[];
}

export interface ICommissionPreViewBody extends ICommission {
  handler?: CommissionHandlerType;
}
export type CommissionHandlerType = (
  commission: ICommissionPreViewBody
) => void;
export interface ICommissionPreViewBodyDisplay extends ICommissionPreViewBody {
  isSelected: boolean;
}
