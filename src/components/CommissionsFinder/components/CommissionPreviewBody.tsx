import { Tooltip, Typography } from "@material-ui/core";
import React from "react";
import { isMobile } from "../../../const/Size";
import { palette } from "../../../const/style/palette";
import { ICommissionPreViewBodyDisplay } from "../Models";

const CommissionPreviewBody = ({
  handler = () => {},
  isSelected,
  ...oneCommission
}: ICommissionPreViewBodyDisplay) => {
  return (
    <div
      style={{
        justifyContent: "space-between",
        display: "flex",
        padding: 10,
        backgroundImage: isSelected ? palette.primaryGradient : undefined,
        borderRadius: 3,
        margin: 6,
        borderBottom: "1px solid #ccc",
      }}
      onClick={() => {
        handler(oneCommission);
      }}
    >
      {isMobile ? (
        <div style={{ textAlign: "left" }}>
          <Typography
            variant="subtitle1"
            color={!isSelected ? "initial" : "textPrimary"}
          >
            {oneCommission.name.slice(0, 60)}
          </Typography>
          {oneCommission.description && (
            <>
              <Typography
                variant="caption"
                color={isSelected ? "textPrimary" : "inherit"}
              >
                {oneCommission.description.slice(0, 200)}
              </Typography>
            </>
          )}
        </div>
      ) : (
        <Tooltip
          enterNextDelay={800}
          title={
            oneCommission.description
              ? oneCommission.description.slice(0, 200)
              : "У этого фонда нет описания"
          }
          disableTouchListener
        >
          <Typography
            variant="body2"
            style={{ textAlign: "left" }}
            color={!isSelected ? "initial" : "textPrimary"}
          >
            {oneCommission.name.slice(0, 60)}
          </Typography>
        </Tooltip>
      )}

      <Typography
        variant={isMobile ? "subtitle1" : "inherit"}
        style={{ textAlign: "right" }}
        color={!isSelected ? "initial" : "textPrimary"}
      >
        id: #{oneCommission.id}
      </Typography>
    </div>
  );
};

export default CommissionPreviewBody;
