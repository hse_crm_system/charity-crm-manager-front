import { debounce, FormControl, FormGroup } from "@material-ui/core";
import React, { ChangeEvent, useEffect, useState } from "react";
import { OutlinedTextField } from "../../styledComponents/OutlinedTextField";
import CommissionPreview from "./components/CommissionPreview";
import {
  ICommission,
  ICommissionPreViewBody,
  ICommissionPreViewBodyDisplay,
} from "./Models";
import { groupsApi } from "../../api/groupsApi";
import { useSnackbar } from "notistack";

const CommissionsFinder = ({
  getter,
  isOne = false,
  defaultSelectedCommissions,
}: {
  getter: (data: ICommissionPreViewBody[]) => void;
  isOne?: boolean;
  defaultSelectedCommissions?: ICommissionPreViewBody[];
}) => {
  const [isLoading, setIsLoading] = useState(false);
  const [searchedCommissions, setSearchedCommissions] = useState<
    ICommissionPreViewBody[]
  >([]);
  const [selectedCommissions, setSelectedCommissions] = useState<
    ICommissionPreViewBody[]
  >([]);
  const [commissionsListDisplay, setCommissionsListDisplay] = useState<
    ICommissionPreViewBodyDisplay[]
  >([]);
  const [commissionFinder, setCommissionFinder] = useState<string>("");

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const fetchGroupsList = React.useCallback(
    debounce(async (payload: string) => {
      setSearchedCommissions((await fetchGrops(payload)) || []);
    }, 500),
    []
  );

  const { enqueueSnackbar } = useSnackbar();

  const fetchGrops = async (
    payload: string
  ): Promise<ICommission[] | undefined> => {
    try {
      setIsLoading(true);
      const resp = await groupsApi.fetchGroups(0, 10, payload);
      if (resp) {
        return resp;
      }
    } catch (e: any) {
      console.log(e);
      enqueueSnackbar("Ошибка поиска", {
        variant: "error",
      });
    } finally {
      setIsLoading(false);
    }
  };

  const changeFindCommissionInputHandler = (
    event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setCommissionFinder(event.target.value);
    fetchGroupsList(event.target.value);
  };
  const selectCommissionHandler = (commission: ICommissionPreViewBody) => {
    if (isOne) {
      selectedCommissions.map((x) => x.id).indexOf(commission.id) === -1
        ? setSelectedCommissions([commission])
        : setSelectedCommissions(
            selectedCommissions.slice().filter((x) => x.id !== commission.id)
          );
    } else {
      selectedCommissions.map((x) => x.id).indexOf(commission.id) === -1
        ? setSelectedCommissions([...selectedCommissions, commission])
        : setSelectedCommissions(
            selectedCommissions.slice().filter((x) => x.id !== commission.id)
          );
    }
  };
  useEffect(() => {
    defaultSelectedCommissions?.length &&
      setSelectedCommissions(defaultSelectedCommissions);
    fetchGroupsList("");
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    getter(selectedCommissions);
  }, [getter, selectedCommissions]);
  useEffect(() => {
    setCommissionsListDisplay(
      [
        ...selectedCommissions,
        ...searchedCommissions
          .slice()
          .filter((x) => !selectedCommissions.map((y) => y.id).includes(x.id)),
      ].map((x) => {
        return {
          ...x,
          isSelected: selectedCommissions.indexOf(x) !== -1,
        };
      }) as ICommissionPreViewBodyDisplay[]
    );
  }, [searchedCommissions, selectedCommissions]);
  return (
    <div style={{ width: "100%" }}>
      <FormControl component="fieldset" fullWidth style={{ marginTop: 10 }}>
        <FormGroup aria-label="position" row>
          <OutlinedTextField
            // className={classes.newVoteField}
            label="Фонды"
            InputLabelProps={{
              shrink: true,
            }}
            autoComplete="off"
            value={commissionFinder}
            variant="outlined"
            type="text"
            name="commission"
            fullWidth
            onChange={changeFindCommissionInputHandler}
          />
          <CommissionPreview
            {...{
              data: commissionsListDisplay,
              handler: selectCommissionHandler,
              isLoading,
            }}
          />
        </FormGroup>
      </FormControl>
    </div>
  );
};

export default CommissionsFinder;
