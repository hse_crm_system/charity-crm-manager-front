import { Box, Grid, LinearProgress, Typography } from "@material-ui/core";
import React, { useContext, useEffect, useRef, useState } from "react";
import { applicationsApi } from "../../api/votingsApi";
import { isMobile } from "../../const/Size";
import useFetchOnScroll from "../../hooks/useFetchOnScroll";
import { listSorts } from "../../tools/listSorts";
import Footer from "../Footer/Footer";
import { UserContext } from "../LoginScreen/UserStore";
import { IVoteBlockProps, VoteStatus } from "../VoteBlock/state";
import VoteBlock from "../VoteBlock/VoteBlock";
import HeadSelectBar from "./components/HeadSelectBar";

const VotesReview = () => {
  const { state } = useContext(UserContext);
  const [tabIndex, setTabIndex] = useState(0);
  const loaderRef = useRef<HTMLDivElement>(null);
  const [isVotesFinished, setIsVotesFinished] = useState(false);
  const [allVotes, setAllVotes] = useState<IVoteBlockProps[]>([]);
  const [displayableVotes, setDisplayableVotes] = useState<IVoteBlockProps[][]>(
    [[], []]
  );
  const [currentPage, setCurrentPage] = useState(0);
  const [category, setCategory] = useState(0);
  const [searchTerm, setSearchTerm] = useState<string>("");
  const [resetObserver] = useFetchOnScroll(loaderRef, () => {
    (async () => {
      const fetchedVotes = await applicationsApi.fetchAdminVotings(
        currentPage * 10,
        10,
        tabIndex ? [] : [],
        {
          searchTerm: searchTerm,
          categoryId: category ? category.toString() : undefined,
        }
      );
      fetchedVotes?.length
        ? (() => {
            const otherVotings = fetchedVotes;
            setCurrentPage(currentPage + 1);
            setAllVotes([...allVotes, ...otherVotings]);
          })()
        : (() => {
            resetObserver();
            setIsVotesFinished(true);
          })();
    })();
  });

  useEffect(() => {
    setDisplayableVotes(listSorts.sortToColumns(allVotes, (x) => 1));
    return resetObserver;
  }, [allVotes, resetObserver]);

  useEffect(() => {
    setAllVotes([]);
    setCurrentPage(0);
    setIsVotesFinished(false);
  }, [searchTerm, category]);
  return (
    <>
      <HeadSelectBar
        handler={setTabIndex}
        searchHandler={setSearchTerm}
        categoryHendler={setCategory}
      />
      {isMobile ? (
        displayableVotes.flat().map((vote, index) => (
          <Box marginBottom={3} key={index}>
            <VoteBlock {...vote} key={vote.id} />
          </Box>
        ))
      ) : (
        <Grid container justify="center" spacing={3}>
          <Grid item xs={5}>
            {displayableVotes[0].map((vote) => (
              <Box marginBottom={3} key={vote.id}>
                <VoteBlock {...vote} key={vote.id} isToAdminRoute={true} />
              </Box>
            ))}
          </Grid>
          <Grid item xs={5}>
            {displayableVotes[1].map((vote) => (
              <Box marginBottom={3} key={vote.id}>
                <VoteBlock {...vote} key={vote.id} isToAdminRoute={true} />
              </Box>
            ))}
          </Grid>
        </Grid>
      )}
      {isVotesFinished ? (
        <>
          <Typography>Вы рассмотрели все голосования</Typography>
          <Footer />
        </>
      ) : (
        <div ref={loaderRef}>
          <LinearProgress />
        </div>
      )}
    </>
  );
};

export default VotesReview;
