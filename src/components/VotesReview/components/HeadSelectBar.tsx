import {
  Paper,
  FormControl,
  Select,
  MenuItem,
  TextField,
  Typography,
} from "@material-ui/core";
import React from "react";
import { HeadSelect } from "../../../styledComponents/HeadSelect";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import { debounce } from "lodash";
import { applicationsApi } from "../../../api/votingsApi";

interface IProps {
  handler: (val: number) => void;
  searchHandler: (v: string) => void;
  categoryHendler: (c: number) => void;
}

const HeadSelectBar = ({ handler, searchHandler, categoryHendler }: IProps) => {
  const [allCategories, setAllCategories] = React.useState<
    { id: any; name: any }[]
  >([]);
  const [category, setCategory] = React.useState(-1);

  const [sortType, setSortType] = React.useState<number>(0);
  const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
    if (event.target.value === -1) {
      setSortType(null as unknown as number);
      categoryHendler(null as unknown as number);
      handler(null as unknown as number);
    } else {
      setSortType(event.target.value as number);
      categoryHendler(event.target.value as number);
      handler(event.target.value as number);
    }
    setCategory(event.target.value as number);
  };
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const setSearched = React.useCallback(
    debounce((v: string) => {
      searchHandler(v);
    }, 700),
    []
  );
  const onEditSearch = (e: any) => {
    setSearched(e.target.value);
  };
  React.useEffect(() => {
    (async () => {
      try {
        setAllCategories([
          { id: -1, name: "Все категории" },
          ...(await applicationsApi.getCategories()),
        ]);
      } catch {}
    })();
  }, []);
  return (
    <Paper
      elevation={1}
      style={{
        display: "flex",
        marginBottom: 24,
        paddingTop: 10,
        position: "relative",
        justifyContent: "space-between",
        padding: 10,
      }}
    >
      <div
        style={{
          display: "flex",
        }}
      >
        <div
          style={{
            marginLeft: 10,
            marginRight: 10,
            position: "relative",
            width: 120,
          }}
        >
          <Typography
            variant="body2"
            style={{
              position: "absolute",
              top: "50%",
              transform: "translateY(-50%)",
            }}
          >
            Категория
          </Typography>
        </div>
        <Select
          value={category}
          IconComponent={() => <ArrowDropDownIcon style={{ color: "#222" }} />}
          onChange={handleChange}
          input={<HeadSelect />}
        >
          {allCategories.map((x: any) => (
            <MenuItem value={x.id} key={x.id}>
              {x.name}
            </MenuItem>
          ))}
        </Select>
      </div>
      <FormControl>
        {/* <Select
          value={sortType}
          IconComponent={() => <ArrowDropDownIcon style={{ color: "#222" }} />}
          onChange={handleChange}
          input={<HeadSelect />}
        >
          <MenuItem key={0} value={0}>
            Ожидают модерации
          </MenuItem>
          <MenuItem key={1} value={1}>
            Активные
          </MenuItem>
        </Select> */}
        <TextField label="Поиск" onChange={onEditSearch} />
      </FormControl>
    </Paper>
  );
};

export default HeadSelectBar;
