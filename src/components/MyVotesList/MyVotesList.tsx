import React from "react";
import MyVotesStatistic from "../MyVotesStatistic/MyVotesStatistic";

const MyVotesList = () => {
  const [votesStatKey] = React.useState(false);
  return (
    <>
      <MyVotesStatistic key={+votesStatKey} />
    </>
  );
};

export default MyVotesList;
