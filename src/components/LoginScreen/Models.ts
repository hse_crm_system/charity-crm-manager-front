export interface ILoginForm {
  email: string;
  password: string;
}

export interface ILoggedUser {
  email?: string;
  role: EUserRoles[];
  name?: string;
  sub: string;
}
export interface IAction {
  type: string;
  payload?: any;
}

export interface ILoggedUserStore {
  tokens?: ITokens;
  userData?: ILoggedUser;
}

export interface ITokens {
  accessToken: string;
  refreshToken: string;
}

export enum EUserRoles {
  CRM_ADMIN = "crm_admin",
  CRM_FUND_MANAGER = "crm_fund_manager",
  CEM_MANAGER = "crm_crm_manager",
}

export enum ELoadingState {
  LOADING = "LOADING",
  LOADED = "LOADED",
  NEVER = "NEVER",
  ERROR = "ERROR",
  INIT = "INIT",
}
export interface IFetchingData<T> {
  LodingState: ELoadingState;
  data?: T;
  errorMessage?: string;
}
