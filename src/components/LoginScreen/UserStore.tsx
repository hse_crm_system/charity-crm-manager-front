import React, { Dispatch, useContext, useReducer } from "react";
import { ELoadingState, IAction, IFetchingData, ILoggedUserStore } from "./Models";
import { userReducer } from "./Reducer";

export const UserContext = React.createContext<{
  state: IFetchingData<ILoggedUserStore>;
  dispatch: Dispatch<IAction>;
}>({
  state: {
    LodingState: ELoadingState.INIT,
  },
  dispatch: () => {},
});

export const useUserData = () => {
  const context = useContext(UserContext);
  return context?.state.data;
};

export const UserStore = ({ children }: { children: React.ReactNode }) => {
  const [state, dispatch] = useReducer(userReducer, {
    LodingState: ELoadingState.INIT,
  });
  return (
    <UserContext.Provider value={{ state, dispatch }}>
      {children}
    </UserContext.Provider>
  );
};
