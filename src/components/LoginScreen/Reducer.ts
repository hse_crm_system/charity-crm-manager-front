import {
  ELoadingState,
  IAction,
  IFetchingData,
  ILoggedUserStore,
} from "./Models";

export const userReducer = (
  state: IFetchingData<ILoggedUserStore>,
  action: IAction
): IFetchingData<ILoggedUserStore> => {
  switch (action.type) {
    case "LOGOUT":
      localStorage.clear();
      return { LodingState: ELoadingState.NEVER };
    case "SET_USER_DATA":
      return {
        LodingState: ELoadingState.LOADED,
        data: { ...state.data, userData: action.payload },
      };
    case "SET_TOKENS":
      return { ...state, data: { ...state.data, tokens: action.payload } };
    case "SET_LOADING_STATE":
      return { ...state, LodingState: action.payload };
    case "ERROR":
      return {
        LodingState: ELoadingState.ERROR,
        errorMessage: action.payload,
      };
    default:
      return state;
  }
};
