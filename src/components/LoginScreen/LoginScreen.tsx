import {
  Button,
  Container,
  FormControl,
  FormGroup,
  Paper,
  Typography,
} from "@material-ui/core";
import axios from "axios";
import { useSnackbar } from "notistack";
import React from "react";
import { ChangeEvent, useContext, useState } from "react";
import { useHistory } from "react-router";
import { parseJwt } from "../../api/tools/parseJWT";
import { userApi } from "../../api/userApi";
import { RedirectLinks } from "../../const/RedirectLinks";
import { OutlinedTextField } from "../../styledComponents/OutlinedTextField";
import Headline from "../Headline/Headline";
import {
  ELoadingState,
  EUserRoles,
  ILoggedUser,
  ILoginForm,
  ITokens,
} from "./Models";
import { UserContext } from "./UserStore";

const LoginScreen = () => {
  const { state, dispatch } = useContext(UserContext);
  const history = useHistory();
  const { enqueueSnackbar } = useSnackbar();
  const [isLoading, setIsLoading] = useState(false);
  const [loginForm, setLoginForm] = useState<ILoginForm>({
    email: "",
    password: "",
  });
  React.useEffect(() => {
    isLoading !== (state.LodingState === ELoadingState.LOADING) &&
      setIsLoading(state.LodingState === ELoadingState.LOADING);
  }, [isLoading, state.LodingState]);
  const sendLoginData = (event: any) => {
    event?.preventDefault();
    (async () => {
      try {
        dispatch({ type: "SET_LOADING_STATE", payload: ELoadingState.LOADING });
        const resp = await userApi.login(loginForm);
        const tokens: ITokens = {
          accessToken: resp.access_token,
          refreshToken: resp.refresh_token,
        };
        if (tokens) {
          axios.defaults.headers.common[
            "Authorization"
          ] = `Bearer ${tokens.accessToken}`;
          localStorage.setItem("accessToken", tokens.accessToken);
          localStorage.setItem("refreshToken", tokens.refreshToken);
          dispatch({ type: "SET_TOKENS", payload: tokens });
          const userData: ILoggedUser = parseJwt(tokens.accessToken);
          dispatch({
            type: "SET_USER_DATA",
            payload: userData,
          });
          dispatch({
            type: "SET_LOADING_STATE",
            payload: ELoadingState.LOADED,
          });
          enqueueSnackbar("Success", { variant: "success" });
          if (userData?.role.includes(EUserRoles.CRM_ADMIN)) {
            history.push(RedirectLinks.APPLICATIONS_REVIEW);
          } else if (userData?.role.includes(EUserRoles.CRM_FUND_MANAGER)) {
            history.push(RedirectLinks.VOTES_PARTICIPATE_LIST);
          } else {
            history.push(RedirectLinks.MY_VOTES_LIST);
          }
        }
      } catch (e: any) {
        console.log(e);
        dispatch({ type: "ERROR", payload: "Неверные данные" });
        dispatch({ type: "SET_LOADING_STATE", payload: ELoadingState.ERROR });
        e?.message && enqueueSnackbar("Неверные данные", { variant: "error" });
      }
    })();
  };
  const changeLoginFormHandler = (
    event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setLoginForm({
      ...loginForm,
      ...{ [event.target.name]: event.target.value },
    });
  };
  return (
    <Container maxWidth="sm">
      <Headline text={"Авторизация"} />
      <Paper>
        <form onSubmit={sendLoginData}>
          <FormControl component="fieldset" fullWidth>
            <FormGroup aria-label="position" row style={{ margin: 10 }}>
              <OutlinedTextField
                style={{ marginTop: 15 }}
                label="Username"
                InputLabelProps={{
                  shrink: true,
                }}
                autoComplete="off"
                value={loginForm.email}
                variant="outlined"
                type="text"
                name="email"
                fullWidth
                onChange={changeLoginFormHandler}
              />
              <OutlinedTextField
                style={{ marginTop: 15 }}
                label="Пароль"
                InputLabelProps={{
                  shrink: true,
                }}
                autoComplete="off"
                value={loginForm.password}
                variant="outlined"
                type="password"
                name="password"
                fullWidth
                onChange={changeLoginFormHandler}
              />
              <Button
                style={{ marginTop: 15 }}
                onClick={sendLoginData}
                variant="contained"
                color="primary"
                type="submit"
                disableElevation
              >
                <Typography variant="caption">
                  {isLoading ? "..." : "Войти"}
                </Typography>
              </Button>
            </FormGroup>
          </FormControl>
        </form>
      </Paper>
    </Container>
  );
};

export default LoginScreen;
