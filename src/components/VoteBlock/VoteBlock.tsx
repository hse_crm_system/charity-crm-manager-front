import {
  Box,
  Button,
  ButtonGroup,
  Dialog,
  Grid,
  IconButton,
  LinearProgress,
  Paper,
  TextField,
  Tooltip,
  Typography,
} from "@material-ui/core";
import FileCopyIcon from "@material-ui/icons/FileCopy";
import { useSnackbar } from "notistack";
import React, { ChangeEvent, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { applicationsApi } from "../../api/votingsApi";
import { RedirectLinks } from "../../const/RedirectLinks";
import { palette } from "../../const/style/palette";
import { theme } from "../../const/style/theme";
import { OutlinedTextField } from "../../styledComponents/OutlinedTextField";
import { StyledBadge } from "../../styledComponents/StyledBadge";
import CommissionsFinder from "../CommissionsFinder/CommissionsFinder";
import { ICommissionPreViewBody } from "../CommissionsFinder/Models";
import { EUserRoles } from "../LoginScreen/Models";
import { UserContext, useUserData } from "../LoginScreen/UserStore";
import { IFile, IVoteBlockProps, VoteResult, VoteStatus } from "./state";
import { Animated } from "react-animated-css";
import "./voteBlockStyle.css";
import VerifiedUserIcon from "@material-ui/icons/VerifiedUser";
import { apiUrl } from "../../api/consts";
import { min } from "lodash";
import { userApi } from "../../api/userApi";

const VoteBlock = (votingProps: IVoteBlockProps) => {
  const [currentVote, setCurrentVote] = useState<IVoteBlockProps>(votingProps);
  const [isVoteHiddenFull, setIsVoteHiddenFull] = useState(false);
  const { state } = React.useContext(UserContext);
  const [isDeclineDialogOpen, setIsDeclineDialogOpen] = useState(false);
  const inputFileRef = React.useRef<HTMLInputElement>(null);
  const inputReportFileRef = React.useRef<HTMLInputElement>(null);
  const [isDeleteDialogOpened, setIsDeleteDialogOpened] = React.useState(false);
  const [myMessage, setMyMessage] = React.useState<{
    text: string;
    media: IFile[];
  }>({ text: "", media: [] });
  const [myReport, setMyReport] = React.useState<{
    description: string;
    title: string;
    media: IFile[];
  }>({ description: "", title: "", media: [] });
  const [declineComment, setDeclineComment] = useState("");
  const [selectedCommissions, setSelectedCommissions] = useState<
    ICommissionPreViewBody[]
  >([]);
  const [isFileUploadLoading, setIsFileUploadLoading] = useState(false);
  const [isReportFileUploadLoading, setIsReportFileUploadLoading] =
    useState(false);
  const userData = useUserData();
  const [isSelectingCommissions, setIsSelectingCommissions] = useState(false);
  const history = useHistory();
  const { enqueueSnackbar } = useSnackbar();
  const redirectOn = (link: string) => {
    history.push(link);
  };
  const changeDeclineDialog = () => {
    setIsDeclineDialogOpen(!isDeclineDialogOpen);
  };
  const uploadFileBtnClick = () => {
    inputFileRef.current?.click();
  };
  const uploadReportFileBtnClick = () => {
    inputReportFileRef.current?.click();
  };
  const loadUploadedFiles = async (event: ChangeEvent<HTMLInputElement>) => {
    try {
      setIsFileUploadLoading(true);
      const files: File[] = Array.from(event.target.files || []);
      const resp = await userApi.uploadFiles(files);
      if (!resp) {
        enqueueSnackbar("Ошибка", {
          variant: "error",
        });
        return;
      } else {
        setMyMessage({ ...myMessage, media: resp });
      }
    } catch (e: any) {
      console.log(e);
      enqueueSnackbar("Произошла какая-то ошибка", {
        variant: "error",
      });
    } finally {
      setIsFileUploadLoading(false);
    }
  };

  const loadReportUploadedFiles = async (
    event: ChangeEvent<HTMLInputElement>
  ) => {
    try {
      setIsReportFileUploadLoading(true);
      const files: File[] = Array.from(event.target.files || []);
      const resp = await userApi.uploadFiles(files);
      if (!resp) {
        enqueueSnackbar("Ошибка", {
          variant: "error",
        });
        return;
      } else {
        setMyReport({ ...myReport, media: resp });
      }
    } catch (e: any) {
      console.log(e);
      enqueueSnackbar("Произошла какая-то ошибка", {
        variant: "error",
      });
    } finally {
      setIsReportFileUploadLoading(false);
    }
  };

  const sendMessage = () => {
    (async () => {
      try {
        if (!myMessage.text) {
          return;
        }
        const resp = await applicationsApi.chatSendMessage({
          ...myMessage,
          applicationId: currentVote.id,
        });
        if (!resp) {
          enqueueSnackbar("Ошибка", {
            variant: "error",
          });
          return;
        } else {
          setCurrentVote({
            ...currentVote,
            chat: [
              {
                id: 0,
                applicationId: parseInt(currentVote.id),
                userId: state.data?.userData?.sub,
                fullName: state.data?.userData?.name,
                username: state.data?.userData?.name,
                message: myMessage.text,
                creationTime: new Date().toJSON(),
                media: { files: resp as IFile[] },
              },
              ...(currentVote.chat || []),
            ],
          });
          setMyMessage({ text: "", media: [] });
        }
      } catch {
        enqueueSnackbar("Произошла какая-то ошибка", {
          variant: "error",
        });
      }
    })();
  };

  const sendReoprtMessage = () => {
    (async () => {
      try {
        if (!myReport.title) {
          enqueueSnackbar("Вы не заполнили название отчёта", {
            variant: "error",
          });
          return;
        }
        if (!myReport.description) {
          enqueueSnackbar("Вы не заполнили описание отчёта", {
            variant: "error",
          });
          return;
        }
        const resp = await applicationsApi.sendReport({
          ...myReport,
          applicationId: currentVote.id,
        });
        if (!resp) {
          enqueueSnackbar("Ошибка", {
            variant: "error",
          });
          return;
        } else {
          setCurrentVote({
            ...currentVote,
            reports: [
              ...(currentVote.reports || []),
              {
                id: parseInt(new Date().toString()),
                applicationId: parseInt(currentVote.id),
                title: myReport.title,
                description: myReport.description,
                creationTime: new Date().toJSON(),
                files: { files: resp as IFile[] },
              },
            ],
          });
          setMyReport({ description: "", title: "", media: [] });
        }
      } catch {
        enqueueSnackbar("Произошла какая-то ошибка", {
          variant: "error",
        });
      }
    })();
  };

  const changeIsDialogOpen = () => {
    setIsDeleteDialogOpened(!isDeleteDialogOpened);
  };
  // const setIsVotingHidden = (val: boolean) => {
  //   setCurrentVote({ ...currentVote, isHidden: val });
  // };
  // const declineVotingHandler = () => {
  //   (async () => {
  //     try {
  //       const isSuccess = await applicationsApi.declineVoting(
  //         currentVote?.id,
  //         declineComment
  //       );
  //       if (!isSuccess) {
  //         enqueueSnackbar("Ошибка отмены голосования", {
  //           variant: "error",
  //         });
  //         return;
  //       }
  //       setCurrentVote({
  //         ...currentVote,
  //         status: VoteStatus.MODERATION_CLOSED,
  //       });
  //     } catch {
  //       enqueueSnackbar("Что-то пошло не так", {
  //         variant: "error",
  //       });
  //     } finally {
  //       changeDeclineDialog();
  //     }
  //   })();
  // };
  const voteHandler = (isApproved: boolean) => {
    (async () => {
      try {
        const isSuccess = await (isApproved
          ? applicationsApi.pickApplication(currentVote.id)
          : applicationsApi.declineVoting(currentVote.id));
        if (!isSuccess) {
          enqueueSnackbar("Ошибка", {
            variant: "error",
          });
          return;
        }
        setCurrentVote({ ...currentVote, isHidden: isSuccess });
        enqueueSnackbar("Успех", {
          variant: "success",
        });
        if (currentVote.isFull) {
          history.push("/");
        }
      } catch {
        enqueueSnackbar("Произошла какая-то ошибка", {
          variant: "error",
        });
      }
    })();
  };
  const deleteVoteHandler = () => {
    (async () => {
      try {
        const isSuccess = await applicationsApi.cancelVoting(
          currentVote?.id,
          state.data?.userData?.sub
        );
        if (!isSuccess) {
          enqueueSnackbar("Ошибка удаления голосования", {
            variant: "error",
          });
          return;
        }
        history.push("/");
      } catch {
        enqueueSnackbar("Произошла неизвестная ошибка", {
          variant: "error",
        });
      } finally {
        changeIsDialogOpen();
      }
    })();
  };

  React.useEffect(() => {
    setTimeout(() => {
      if (currentVote.isHidden) {
        setIsVoteHiddenFull(currentVote.isHidden);
      }
    }, 1000);
  }, [currentVote.isHidden]);
  if (isVoteHiddenFull) {
    return <div />;
  }
  return (
    <Animated
      animationIn="fadeIn"
      animationOut="zoomOutDown"
      // animationInDuration={0}
      animationOutDuration={1000}
      isVisible={!currentVote.isHidden}
    >
      <Paper
        className="vote-block-wrapper"
        style={currentVote?.isFull ? { textAlign: "left" } : undefined}
        elevation={4}
      >
        {currentVote?.blockchainEnabled && (
          <Box position="absolute" top={10} left={15}>
            <Tooltip title="Blockchain Enabled">
              <VerifiedUserIcon style={{ color: "#CCC" }} />
            </Tooltip>
          </Box>
        )}
        {currentVote?.categoryName && (
          <Typography variant="caption" color="textSecondary">
            {"Категория: " + currentVote.categoryName}
          </Typography>
        )}
        {currentVote.isToAdminRoute && currentVote?.foundationName && (
          <Typography variant="body2" color="textSecondary">
            {"Фонд: " + currentVote.foundationName}
          </Typography>
        )}

        {currentVote.isToAdminRoute && currentVote?.status && (
          <Typography variant="body2" color="textSecondary">
            {"Статус: " +
              (() => {
                switch (currentVote?.status) {
                  case "Created":
                    return "Фонды рассматривают заявку";
                  case "NeedsSpecification":
                    return "Требует дополнения";
                  case "WaitsForVoting":
                    return "Отправлена на голосование";
                  case "Fundraising":
                    return "Идет сбор средств";
                  case "Implemented":
                    return "Выполнена";
                  case "WaitsForImplementation":
                    return "Ожидает исполнения";
                  case "FoundationsRefused":
                    return "Ни один фонд не выбрал Вашу заявку";
                  case "DeclinedPermanently":
                    return "Полностью отменена";
                  case "Cancelled":
                    return "Отменена";
                  default:
                    return "Неизвестный статус";
                }
              })()}
          </Typography>
        )}
        <Typography
          variant={currentVote?.isFull ? "h4" : "h5"}
          className="vote-block-headline"
          onClickCapture={() => {
            redirectOn(
              (currentVote.isToAdminRoute
                ? RedirectLinks.ADMIN_ONE_VOTE
                : RedirectLinks.ONE_VOTE) + currentVote?.id
            );
          }}
        >
          {currentVote?.headline}
        </Typography>

        {!isNaN(currentVote?.dateEnd?.getHours()) &&
          currentVote?.dateEnd instanceof Date && (
            <Typography variant="inherit" color="textSecondary">
              по {currentVote.dateEnd?.toLocaleDateString()}
            </Typography>
          )}
        <br />
        {currentVote?.foundationName && (
          <Typography variant="caption" color="textSecondary">
            {currentVote.foundationName}
          </Typography>
        )}
        {currentVote?.expectedSum && (
          <Box
            marginY={3}
            paddingX={2}
            paddingY={1}
            borderRadius={3}
            style={{ backgroundColor: "#FFF", border: "2px solid #ccc" }}
          >
            <Typography variant="subtitle2" color="textSecondary">
              {"Сумма сбора: " +
                (currentVote.raisedSum?.toString()
                  ? currentVote.raisedSum + " из "
                  : "") +
                currentVote.expectedSum}
            </Typography>
            {(currentVote?.raisedSum?.toString() || currentVote.isFull) && (
              <Box marginTop={2}>
                <LinearProgress
                  variant="determinate"
                  value={min([
                    ((currentVote?.raisedSum || 1) /
                      (currentVote.expectedSum || 1)) *
                      100,
                    100,
                  ])}
                />
              </Box>
            )}
          </Box>
        )}
        {currentVote.isFull && (
          <Typography
            className="vote-block-description"
            style={{ textAlign: "left" }}
          >
            {currentVote?.description}
          </Typography>
        )}
        {/* {currentVote?.commissions &&
          currentVote?.isFull &&
          currentVote?.commissions.map((commission) => (
            <Typography variant="body2">{commission.name}</Typography>
          ))} */}
        {currentVote.isFull && (
          <>
            <div style={{ overflowX: "hidden", display: "flex" }}>
              {currentVote.files?.files?.map((x, index) => (
                <div style={{ textAlign: "center" }}>
                  <IconButton
                    href={`${apiUrl}/${x.path}`}
                    target="_blank"
                    rel="nofollow noopener"
                    style={{ marginRight: 7 }}
                  >
                    <FileCopyIcon />
                  </IconButton>
                  <Typography>
                    {index +
                      "." +
                      (x.uploadName.split(".").slice()?.pop() || "")}
                  </Typography>
                </div>
              ))}
            </div>
            <div>
              {currentVote.reports && currentVote.reports.length ? (
                <Box marginTop={4}>
                  <Typography variant="overline">Отчёты</Typography>
                  {currentVote.reports.map((t) => (
                    <Box
                      marginBottom={2}
                      marginTop={1}
                      style={{ borderTop: "2px solid #ccc" }}
                    >
                      <Typography variant="h6" style={{ marginTop: 10 }}>
                        {t.title}
                        <span style={{ fontWeight: 400, fontSize: 14 }}>
                          {" - " +
                            new Date(
                              Date.parse(t.creationTime)
                            ).toLocaleString()}
                        </span>
                      </Typography>
                      <Typography
                        style={{
                          whiteSpace: "pre-wrap",
                        }}
                      >
                        {t.description}
                      </Typography>
                      <div
                        style={{
                          overflowX: "hidden",
                          display: "flex",
                          marginBottom: 30,
                        }}
                      >
                        {t.files?.files?.map((x, index) => (
                          <div style={{ textAlign: "center" }}>
                            <IconButton
                              href={`${apiUrl}/${x.path}`}
                              target="_blank"
                              rel="nofollow noopener"
                              style={{ marginRight: 7 }}
                            >
                              <FileCopyIcon />
                            </IconButton>
                            <Typography>
                              {index +
                                "." +
                                (x.uploadName.split(".").slice()?.pop() || "")}
                            </Typography>
                          </div>
                        ))}
                      </div>
                    </Box>
                  ))}
                </Box>
              ) : (
                <Box />
              )}
              {[
                VoteStatus.WAITS_FOR_IMPLEMENTATION,
                VoteStatus.WAITING_FOR_VOTINGS,
                VoteStatus.IMPLEMENTED,
              ].includes(currentVote.status) && (
                <Box marginY={4} marginX={2}>
                  <Grid container spacing={2}>
                    <Grid item md={4} xs={12}>
                      <TextField
                        variant="outlined"
                        fullWidth
                        value={myReport.title}
                        onChange={(e: any) =>
                          setMyReport({ ...myReport, title: e.target.value })
                        }
                        placeholder="Заголовок отчёта"
                      />
                      <Box marginTop={3}>
                        <Button
                          color="primary"
                          onClick={uploadReportFileBtnClick}
                          fullWidth
                        >
                          Добавить вложения
                        </Button>
                        <input
                          type="file"
                          multiple
                          ref={inputReportFileRef}
                          style={{ display: "none" }}
                          onChange={loadReportUploadedFiles}
                        />
                        {isReportFileUploadLoading ? (
                          <LinearProgress />
                        ) : (
                          <div style={{ overflowX: "hidden", display: "flex" }}>
                            {myReport.media?.map((x, index) => (
                              <div style={{ textAlign: "center" }}>
                                <IconButton
                                  href={`${apiUrl}/${x.path}`}
                                  target="_blank"
                                  rel="nofollow noopener"
                                  style={{ color: "#222", marginRight: 7 }}
                                >
                                  <FileCopyIcon />
                                </IconButton>
                                <Typography style={{ color: "#222" }}>
                                  {index +
                                    "." +
                                    (x.uploadName.split(".").slice()?.pop() ||
                                      "")}
                                </Typography>
                              </div>
                            ))}
                          </div>
                        )}
                        <Button
                          style={{ marginTop: 10 }}
                          variant="outlined"
                          color="primary"
                          fullWidth
                          onClick={sendReoprtMessage}
                          disabled={isReportFileUploadLoading}
                        >
                          Отправить отчёт
                        </Button>
                      </Box>
                    </Grid>
                    <Grid item md={8} xs={12}>
                      <TextField
                        variant="outlined"
                        multiline
                        fullWidth
                        value={myReport.description}
                        onChange={(e: any) =>
                          setMyReport({
                            ...myReport,
                            description: e.target.value,
                          })
                        }
                        rows={7}
                        placeholder="Описание отчёта"
                      />
                    </Grid>
                  </Grid>
                </Box>
              )}
            </div>
            <Box marginY={2}>
              {currentVote.isToAdminRoute && (
                <Link
                  to={`/blockchain?applicationId=${currentVote.id}&structure=Transactions&isAll=mysecretshowall&access=${userData?.tokens?.accessToken}`}
                  style={{ textDecoration: "none" }}
                >
                  <Typography variant="body2" style={{ color: "#AAA" }}>
                    Transactions History
                  </Typography>
                </Link>
              )}
              <Link
                to={`/blockchain?applicationId=${
                  currentVote.id
                }&structure=Application&isAll=${
                  currentVote.isToAdminRoute && "mysecretshowall"
                }`}
                style={{ textDecoration: "none" }}
              >
                <Typography variant="body2" style={{ color: "#AAA" }}>
                  Application History
                </Typography>
              </Link>
            </Box>
          </>
        )}
        {!currentVote.isToAdminRoute &&
          currentVote?.status === VoteStatus.CREATED && (
            <ButtonGroup
              className="vote-block-wrapper__button-group"
              variant="contained"
              aria-label="contained primary button group"
              disableElevation
              color="primary"
            >
              <Button
                style={{ backgroundColor: palette.acceptVote }}
                onClick={() => {
                  voteHandler(true);
                }}
              >
                Взять в работу
              </Button>
              <Button
                style={{ backgroundColor: palette.rejectVote }}
                onClick={() => {
                  voteHandler(false);
                }}
              >
                Отказ
              </Button>
            </ButtonGroup>
          )}
        {/* {currentVote.isToAdminRoute && (
          <ButtonGroup
            className="vote-block-wrapper__button-group"
            variant="contained"
            aria-label="contained primary button group"
            disableElevation
            color="primary"
          >
            <Button
              style={{ backgroundColor: palette.rejectVote }}
              onClick={() => {
                changeIsDialogOpen();
              }}
            >
              Отменить заявку
            </Button>
          </ButtonGroup>
        )} */}
        {/* {currentVote?.status === VoteStatus.MY_VOTE_IN_PROGRESS &&
          currentVote?.resultsProcents && (
            <div>
              <ButtonGroup
                disableRipple
                className="vote-block-wrapper__button-group"
                variant="contained"
                aria-label="outlined primary button group"
                disableElevation
              >
                <Button disabled={currentVote?.results !== VoteResult.ACCEPT}>
                  {currentVote?.resultsProcents.acceptProcent + "%"}
                </Button>
                <Button disabled={currentVote?.results !== VoteResult.SKIP}>
                  {currentVote?.resultsProcents.skipProcent + "%"}
                </Button>
                <Button
                  disabled={currentVote?.results !== VoteResult.REJECT}
                  color="primary"
                >
                  {currentVote?.resultsProcents.rejectProcent + "%"}
                </Button>
              </ButtonGroup>
              <Typography variant="body2" className="vote-block-state-text">
                Голосование сейчас идет
              </Typography>
            </div>
          )} */}
        {/* {currentVote?.status === VoteStatus.ON_MODERATION && (
          <div style={{ marginTop: 10 }}>
            {!isSelectingCommissions ? (
              <Button
                onClick={() => {
                  setIsSelectingCommissions(true);
                }}
                color="primary"
                fullWidth
              >
                Выбрать комиссии
              </Button>
            ) : (
              <>
                <Typography variant="body2">
                  Выберите комиссии, участники которых смугут принять участие в
                  голосовании
                </Typography>
                <div style={{ marginTop: 15 }}>
                  <CommissionsFinder
                    getter={setSelectedCommissions}
                    defaultSelectedCommissions={
                      currentVote?.commissions?.map((x) => ({
                        ...x,
                        id: x.id.toString(),
                      })) || []
                    }
                  />
                </div>
              </>
            )}
            <ButtonGroup
              className="vote-block-wrapper__button-group"
              variant="contained"
              aria-label="contained primary button group"
              color="primary"
              disableElevation
            >
              <Button
                style={{ backgroundColor: palette.acceptVote }}
                onClick={acceptVotingHandler}
              >
                На голосование
              </Button>

              <Button
                style={{ backgroundColor: palette.rejectVote }}
                onClick={changeDeclineDialog}
              >
                Отказ
              </Button>
            </ButtonGroup>
          </div>
        )} */}
        {/* {currentVote?.status === VoteStatus.MY_VOTE_ON_MODERATION && (
          <Typography
            variant="body2"
            className="vote-block-state-text"
            color="primary"
            style={{
              backgroundColor: "#FFF",
              borderRadius: 5,
              fontWeight: "bold",
            }}
          >
            Ваше голосование сейчас на модерации
          </Typography>
        )}
        {currentVote?.status === VoteStatus.CANCELED && (
          <Typography
            variant="body2"
            className="vote-block-state-text"
            color="error"
            style={{
              backgroundColor: "#FFF",
              borderRadius: 5,
              fontWeight: "bold",
            }}
          >
            Голосование было отменено
          </Typography>
        )}
        {currentVote?.status === VoteStatus.DECLINED && (
          <Typography
            variant="body2"
            className="vote-block-state-text"
            color="error"
            style={{
              backgroundColor: "#FFF",
              borderRadius: 5,
              fontWeight: "bold",
            }}
          >
            Голосование не прошло модерацию
          </Typography>
        )}
        {currentVote?.status === VoteStatus.MODERATION_CLOSED && (
          <Typography
            variant="body2"
            className="vote-block-state-text"
            style={{
              backgroundColor: "#FFF",
              borderRadius: 5,
              fontWeight: "bold",
            }}
          >
            Ваше решение учтено
          </Typography>
        )}
        {currentVote?.status === VoteStatus.APPROVED && (
          <div>
            <ButtonGroup
              className="vote-block-wrapper__button-group"
              variant="text"
              disabled
              aria-label="text primary button group"
              disableElevation
            >
              <Button>Да</Button>
              <Button>Воздержусь</Button>
              <Button>Нет</Button>
            </ButtonGroup>
            <Typography
              variant="body2"
              className="vote-block-state-text"
              color="initial"
              style={{
                backgroundColor: palette.background,
                marginTop: 8,
                borderRadius: 5,
              }}
            >
              Голосование еще не началось
            </Typography>
          </div>
        )} */}
        {/* {currentVote?.status === VoteStatus.FINISHED &&
          currentVote?.resultsProcents && (
            <div>
              <Tooltip
                title={
                  currentVote?.myVote
                    ? `Вы проголосовали: ${currentVote?.myVote}`
                    : "Вы не голосовали"
                }
              >
                <ButtonGroup
                  disableRipple
                  className="vote-block-wrapper__button-group"
                  variant="contained"
                  aria-label="outlined primary button group"
                  disableElevation
                >
                  <Button disabled={currentVote?.results !== VoteResult.ACCEPT}>
                    <StyledBadge
                      badgeContent={
                        currentVote?.resultsProcents.acceptProcent + "%"
                      }
                      color="secondary"
                    >
                      Да
                    </StyledBadge>
                  </Button>
                  <Button disabled={currentVote?.results !== VoteResult.SKIP}>
                    <StyledBadge
                      badgeContent={
                        currentVote?.resultsProcents.skipProcent + "%"
                      }
                      color="secondary"
                    >
                      Воздержусь
                    </StyledBadge>
                  </Button>
                  <Button
                    disabled={currentVote?.results !== VoteResult.REJECT}
                    color="primary"
                  >
                    <StyledBadge
                      badgeContent={
                        currentVote?.resultsProcents.rejectProcent + "%"
                      }
                      color="secondary"
                    >
                      Нет
                    </StyledBadge>
                  </Button>
                </ButtonGroup>
              </Tooltip>
              <Typography variant="body2" className="vote-block-state-text">
                Завершено
              </Typography>
            </div>
          )} */}
        {/* {currentVote?.status === VoteStatus.VOTED && (
          <div>
            <ButtonGroup
              disableRipple
              className="vote-block-wrapper__button-group"
              variant="text"
              aria-label="text secondary button group"
            >
              <Button disabled={currentVote?.myVote !== VoteResult.ACCEPT}>
                <Typography
                  color={
                    currentVote?.myVote === VoteResult.ACCEPT
                      ? "initial"
                      : "inherit"
                  }
                >
                  Да
                </Typography>
              </Button>
              <Button disabled={currentVote?.myVote !== VoteResult.SKIP}>
                <Typography
                  color={
                    currentVote?.myVote === VoteResult.SKIP
                      ? "initial"
                      : "inherit"
                  }
                >
                  Воздержусь
                </Typography>
              </Button>
              <Button disabled={currentVote?.myVote !== VoteResult.REJECT}>
                <Typography
                  color={
                    currentVote?.myVote === VoteResult.REJECT
                      ? "initial"
                      : "inherit"
                  }
                >
                  Нет
                </Typography>
              </Button>
            </ButtonGroup>
            <Typography variant="body2" className="vote-block-state-text">
              Ваш голос учтён
            </Typography>
          </div>
        )} */}
      </Paper>
      <Dialog open={isDeleteDialogOpened} onClose={changeIsDialogOpen}>
        <Paper>
          <Box margin={3}>
            <Typography>Вы точно хотите удалить голосование?</Typography>
            <Box marginTop={2}>
              <ButtonGroup
                variant="text"
                color="primary"
                aria-label="text primary button group"
              >
                <Button onClick={changeIsDialogOpen}>Назад</Button>
                <Button onClick={deleteVoteHandler}>Отменить заявку</Button>
              </ButtonGroup>
            </Box>
          </Box>
        </Paper>
      </Dialog>
      {/* <Dialog
        open={isDeclineDialogOpen}
        onClose={changeDeclineDialog}
        fullWidth
      >
        <Paper>
          <Box margin={3}>
            <OutlinedTextField
              label="Комментарий"
              InputLabelProps={{
                shrink: true,
              }}
              autoComplete="off"
              value={declineComment}
              variant="outlined"
              type="text"
              name="commission"
              fullWidth
              onChange={(
                event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
              ) => {
                setDeclineComment(event?.target.value);
              }}
            />
            <Box marginTop={2}>
              <ButtonGroup
                variant="contained"
                color="primary"
                aria-label="contained primary button group"
                disableElevation
              >
                <Button onClick={changeDeclineDialog}>Назад</Button>
                <Button
                  onClick={declineVotingHandler}
                  style={{
                    backgroundColor: theme.palette.error.main,
                  }}
                >
                  Отказ
                </Button>
              </ButtonGroup>
            </Box>
          </Box>
        </Paper>
      </Dialog> */}
      {currentVote.isFull && (
        <Box marginY={4}>
          <Grid container spacing={2}>
            <Grid item md={4} xs={12}>
              <Paper style={{ position: "sticky", top: 20, width: "100%" }}>
                <Box marginY={4} marginX={2}>
                  <TextField
                    variant="outlined"
                    multiline
                    fullWidth
                    value={myMessage.text}
                    onChange={(e: any) =>
                      setMyMessage({ ...myMessage, text: e.target.value })
                    }
                    placeholder="Сообщение..."
                  />
                  <Box marginTop={3}>
                    <Button
                      color="primary"
                      onClick={uploadFileBtnClick}
                      fullWidth
                    >
                      Добавить вложения
                    </Button>
                    <input
                      type="file"
                      multiple
                      ref={inputFileRef}
                      style={{ display: "none" }}
                      onChange={loadUploadedFiles}
                    />
                    {isFileUploadLoading ? (
                      <LinearProgress />
                    ) : (
                      <div style={{ overflowX: "hidden", display: "flex" }}>
                        {myMessage.media?.map((x, index) => (
                          <div style={{ textAlign: "center" }}>
                            <IconButton
                              href={`${apiUrl}/${x.path}`}
                              target="_blank"
                              rel="nofollow noopener"
                              style={{ color: "#222", marginRight: 7 }}
                            >
                              <FileCopyIcon />
                            </IconButton>
                            <Typography style={{ color: "#222" }}>
                              {index +
                                "." +
                                (x.uploadName.split(".").slice()?.pop() || "")}
                            </Typography>
                          </div>
                        ))}
                      </div>
                    )}
                    <Button
                      style={{ marginTop: 10 }}
                      variant="contained"
                      color="primary"
                      fullWidth
                      onClick={sendMessage}
                      disabled={isFileUploadLoading}
                    >
                      Отправить
                    </Button>
                  </Box>
                </Box>
              </Paper>
            </Grid>
            <Grid item md={8} xs={12}>
              <Box>
                {currentVote.chat?.map((x) => {
                  const isMine = state.data?.userData?.sub === x.userId;
                  return (
                    <Box textAlign={"left"}>
                      <Typography
                        variant="caption"
                        style={{ color: "#222", marginLeft: 12 }}
                      >
                        {x.username +
                          " " +
                          new Date(x.creationTime).toLocaleString()}
                      </Typography>
                      <div
                        style={{
                          backgroundColor: isMine
                            ? "#F2F5FF"
                            : theme.palette.background.paper,
                          color: "#222",
                          margin: 10,
                          padding: 20,
                          width: "fit-content",
                          minWidth: 200,
                          textAlign: "left",
                          borderRadius: "6px",
                          whiteSpace: "pre-wrap",
                          fontSize: 16,
                        }}
                      >
                        {x.message}
                        <Box marginTop={2}>
                          <div style={{ overflowX: "hidden", display: "flex" }}>
                            {x.media?.files?.map((x, index) => (
                              <div style={{ textAlign: "center" }}>
                                <IconButton
                                  href={`${apiUrl}/${x.path}`}
                                  target="_blank"
                                  rel="nofollow noopener"
                                  style={{ color: "#222", marginRight: 7 }}
                                >
                                  <FileCopyIcon />
                                </IconButton>
                                <Typography style={{ color: "#222" }}>
                                  {index +
                                    "." +
                                    (x.uploadName.split(".").slice()?.pop() ||
                                      "")}
                                </Typography>
                              </div>
                            ))}
                          </div>
                        </Box>
                      </div>
                    </Box>
                  );
                })}
              </Box>
            </Grid>
          </Grid>
        </Box>
      )}
    </Animated>
  );
};

export default VoteBlock;
