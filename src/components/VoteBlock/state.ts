export interface IChatMessage {
  id: number;
  applicationId: number;
  userId?: string;
  fullName?: string;
  username?: string;
  message: string;
  creationTime: string;
  media?: {
    files?: IFile[];
  };
}
export interface IFile {
  uploadName: string;
  path: string;
}
export interface IVoteBlockProps {
  id: string;
  dateEnd: Date;
  headline: string;
  description?: string;
  status: VoteStatus;
  // commissions?: { name: string; id: string }[];
  // results?: VoteResult;
  categoryName?: string;
  foundationName?: string;
  expectedSum?: number;
  raisedSum?: number;
  isFull?: boolean;
  isHidden?: boolean;
  blockchainEnabled?: boolean;
  files?: {
    files?: IFile[];
  };
  chat?: IChatMessage[];
  isToAdminRoute?: boolean;
  reports?: {
    id: number;
    applicationId: number;
    title: string;
    description: string;
    files: {
      files?: IFile[];
    };
    creationTime: string;
  }[];
}

export enum VoteStatus {
  // FINISHED = "Closed",
  // IN_PROGRESS = "Open",
  // MY_VOTE_IN_PROGRESS = "MY_VOTE_IN_PROGRESS",
  // VOTED = "VOTED",
  // CANCELED = "Cancelled",
  // ON_MODERATION = "Created",
  // APPROVED = "Approved",
  // DECLINED = "Declined",
  // MY_VOTE_ON_MODERATION = "MY_VOTE_ON_MODERATION",
  // MODERATION_CLOSED = "MODERATION_CLOSED",
  CREATED = "Created",
  NEED_SPECIFICATION = "NeedsSpecification",
  WAITING_FOR_VOTINGS = "WaitsForVoting",
  FUNDRAISING = "Fundraising",
  WAITS_FOR_IMPLEMENTATION = "WaitsForImplementation",
  IMPLEMENTED = "Implemented",
  CANCELLED = "Cancelled",
  FoundationsRefused = "FoundationsRefused", // -
  DeclinedPermanently = "DeclinedPermanently", // -
}

export enum VoteResult {
  // SKIP = 0,
  // ACCEPT = 1,
  // REJECT = 2,
  SKIP = "Abstained",
  ACCEPT = "For",
  REJECT = "Against",
}
