import { Fab } from "@material-ui/core";
import React from "react";
import { useHistory } from "react-router";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import { isMobile } from "../../const/Size";

const BackButton = () => {
  const history = useHistory();
  return (
    <Fab
      size="small"
      color="primary"
      style={{
        border: "2px solid #eee",
        position: "fixed",
        top: 20,
        right: 15,
        left: isMobile ? 10 : undefined,
        borderRadius: 3,
        padding: 0,
        zIndex: 999,
        backgroundColor: isMobile ? undefined : "rgba(255, 255, 255, 0.12)",
        textAlign: "center",
      }}
      disableFocusRipple
      aria-label={"Назад"}
      onClick={() => {
        history.goBack();
      }}
    >
      <ArrowBackIosIcon
        color="action"
        style={{ marginLeft: 8, color: isMobile ? "#fff" : undefined }}
      />
    </Fab>
  );
};

export default BackButton;
