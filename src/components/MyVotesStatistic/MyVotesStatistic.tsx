import { Box, Grid, LinearProgress, Typography } from "@material-ui/core";
import React, { useContext, useEffect, useRef, useState } from "react";
import { applicationsApi } from "../../api/votingsApi";
import { isMobile } from "../../const/Size";
import useFetchOnScroll from "../../hooks/useFetchOnScroll";
import { listSorts } from "../../tools/listSorts";
import Footer from "../Footer/Footer";
import { UserContext } from "../LoginScreen/UserStore";
import { IVoteBlockProps, VoteStatus } from "../VoteBlock/state";
import VoteBlock from "../VoteBlock/VoteBlock";

const MyVotesStatistic = () => {
  const { state } = useContext(UserContext);
  const loaderRef = useRef<HTMLDivElement>(null);
  const [currentPage, setCurrentPage] = useState(0);
  const [isVotesFinished, setIsVotesFinished] = useState(false);

  const [allVotes, setAllVotes] = useState<IVoteBlockProps[]>([]);
  const [displayableVotes, setDisplayableVotes] = useState<IVoteBlockProps[][]>(
    [[], []]
  );
  const [resetObserver] = useFetchOnScroll(loaderRef, () => {
    (async () => {
      const fetchedVotes = await applicationsApi.fetchVotings(
        currentPage * 10,
        10,
        [],
        // [
        //   VoteStatus.IN_PROGRESS,
        //   VoteStatus.APPROVED,
        //   VoteStatus.FINISHED,
        //   VoteStatus.CANCELED,
        //   VoteStatus.DECLINED,
        //   VoteStatus.ON_MODERATION,
        // ],
        {
          userId: state.data?.userData?.sub,
          isMyVotesOnModeration: true,
        }
      );
      fetchedVotes?.length
        ? (() => {
            setCurrentPage(currentPage + 1);
            setAllVotes([...allVotes, ...fetchedVotes]);
          })()
        : (() => {
            resetObserver();
            setIsVotesFinished(true);
          })();
    })();
  });

  useEffect(() => {
    setDisplayableVotes(listSorts.sortToColumns(allVotes, (x) => 1));

    return resetObserver;
  }, [allVotes, resetObserver]);

  return (
    <>
      {isMobile ? (
        allVotes.map((vote) => (
          <Box marginBottom={3} key={vote.id}>
            <VoteBlock {...vote} key={vote.id} />
          </Box>
        ))
      ) : (
        <Grid container justify="center" spacing={3}>
                      <Grid item xs={5}>
            {displayableVotes[0].map((vote) => (
              <Box marginBottom={3} key={vote.id}>
                <VoteBlock {...vote} key={vote.id} />
              </Box>
            ))}
          </Grid>
                      <Grid item xs={5}>
            {displayableVotes[1].map((vote) => (
              <Box marginBottom={3} key={vote.id}>
                <VoteBlock {...vote} key={vote.id} />
              </Box>
            ))}
          </Grid>
        </Grid>
      )}
      {isVotesFinished ? (
        <>
          <Typography>Больше нет голосований</Typography>
          <Footer />
        </>
      ) : (
        <div ref={loaderRef}>
          <LinearProgress />
        </div>
      )}
    </>
  );
};

export default MyVotesStatistic;
