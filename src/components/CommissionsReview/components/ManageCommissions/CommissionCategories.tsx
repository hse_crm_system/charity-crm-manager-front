import { Typography } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { IUser } from "../../../AccountInfo/Models";
import CategoryFinder from "../../../CategoryFinder/CategoryFinder";
import { ICategoryPreViewBody } from "../../../CategoryFinder/Models";
import ParticipantsFinder from "../../../ParticipantsFinder/ParticipantsFinder";

const CommissionCategories = ({
  participants,
  handler,
}: {
  participants: ICategoryPreViewBody[];
  handler: (data: ICategoryPreViewBody[]) => void;
}) => {
  const [editableParticipants, setEditableParticipants] =
    useState<ICategoryPreViewBody[]>(participants);
  useEffect(() => {
    handler(editableParticipants);
  }, [editableParticipants, handler]);
  return (
    <div style={{ marginTop: 15, width: "100%" }}>
      <Typography style={{ marginBottom: 10 }} variant="overline">
        Редактирование категорий
      </Typography>
      <CategoryFinder
        {...{
          getter: setEditableParticipants,
          defaultSelectedCommissions: participants,
        }}
        isRatePick
      />
    </div>
  );
};

export default CommissionCategories;
