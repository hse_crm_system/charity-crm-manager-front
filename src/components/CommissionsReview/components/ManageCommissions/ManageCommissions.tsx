import Typography from "@material-ui/core/Typography";
import { useSnackbar } from "notistack";
import React, { useEffect, useState } from "react";
import { groupsApi } from "../../../../api/groupsApi";
import CommissionsFinder from "../../../CommissionsFinder/CommissionsFinder";
import {
  ICommission,
  ICommissionPreViewBody,
} from "../../../CommissionsFinder/Models";
import EditCommission from "./EditCommission";

const ManageCommissions = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [
    selectedCommision,
    setSelectedCommission,
  ] = useState<ICommissionPreViewBody | null>(null);
  const [
    fullSelectedCommission,
    setFullSelectedCommission,
  ] = React.useState<ICommission>();
  const getterSelectCommission = (commissions: ICommissionPreViewBody[]) => {
    if (commissions.length) {
      setSelectedCommission(commissions[0]);
    } else {
      setSelectedCommission(null);
    }
  };
  const { enqueueSnackbar } = useSnackbar();

  useEffect(() => {
    if (selectedCommision) {
      (async() => {
        try {
          setIsLoading(true);
          const commission = await groupsApi.fetchFullGroup(selectedCommision.id);
          if (!commission) {
            enqueueSnackbar("Ошибка получения информации по фонду", {
              variant: "error",
            });
            return;
          }
         setFullSelectedCommission(commission)
        } catch (e:any) {
          console.log(e);
          enqueueSnackbar("Что-то пошло не так", {
            variant: "error",
          });
        } finally {
          setIsLoading(false);
        }
      })();
    }
  }, [enqueueSnackbar, selectedCommision]);
  return (
    <div style={{ marginTop: 0 }}>
      <Typography variant='h6'>Редактирование фондов</Typography>
      <CommissionsFinder getter={getterSelectCommission} isOne />
      {!isLoading && fullSelectedCommission && (
        <EditCommission
          commission={fullSelectedCommission}
          key={fullSelectedCommission.id}
        />
      )}
    </div>
  );
};

export default ManageCommissions;
