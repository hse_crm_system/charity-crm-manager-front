import { Typography } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { IUser } from "../../../AccountInfo/Models";
import ParticipantsFinder from "../../../ParticipantsFinder/ParticipantsFinder";

const CommissionParticipants = ({
  participants,
  handler,
}: {
  participants: IUser[];
  handler: (data: IUser[]) => void;
}) => {
  const [editableParticipants, setEditableParticipants] = useState<IUser[]>(
    participants
  );
  useEffect(() => {
    handler(editableParticipants);
  }, [editableParticipants, handler]);
  return (
    <div style={{ marginTop: 15, width: "100%" }}>
      <Typography style={{ marginBottom: 10 }} variant="overline">
        Редактирование списка участников
      </Typography>
      <ParticipantsFinder
        {...{
          getter: setEditableParticipants,
          commissionParticipants: participants,
        }}
      />
    </div>
  );
};

export default CommissionParticipants;
