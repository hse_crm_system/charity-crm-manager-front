import { FormControl, FormGroup, Button, Typography } from "@material-ui/core";
import { useSnackbar } from "notistack";
import React, { ChangeEvent, useState } from "react";
import { groupsApi } from "../../../../api/groupsApi";
import { OutlinedTextField } from "../../../../styledComponents/OutlinedTextField";
import { IUser } from "../../../AccountInfo/Models";
import { ICategoryPreViewBody } from "../../../CategoryFinder/Models";
import { ICommission } from "../../../CommissionsFinder/Models";
import CommissionCategories from "./CommissionCategories";
import CommissionParticipants from "./CommissionParticipants";

const EditCommission = ({ commission }: { commission: ICommission }) => {
  const [isLoading, setIsLoading] = useState(false);
  const [editableCommission, setEditableCommission] =
    useState<ICommission>(commission);
  let editedParticipants: IUser[] = [];
  let editedCategories: ICategoryPreViewBody[] = [];
  const changeEditableCommissionParticipants = (data: IUser[]) => {
    editedParticipants = data;
  };
  const changeEditableCommissionCategories = (data: ICategoryPreViewBody[]) => {
    editedCategories = data;
  };
  const { enqueueSnackbar } = useSnackbar();

  const saveCommission = () => {
    const editedCommission = {
      ...editableCommission,
      participants: editedParticipants,
      categories: editedCategories
    };
    (async () => {
      try {
        setIsLoading(true);
        const isSuccess = await groupsApi.updateGroup(editedCommission);
        if (!isSuccess) {
          enqueueSnackbar("Ошибка сохранения", {
            variant: "error",
          });
          return;
        }
        enqueueSnackbar("Сохранено", {
          variant: "success",
        });
      } catch (e: any) {
        console.log(e);
        enqueueSnackbar("Что-то пошло не так", {
          variant: "error",
        });
      } finally {
        setIsLoading(false);
      }
    })();
  };
  const changeEditableCommisionHandler = (
    event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setEditableCommission({
      ...editableCommission,
      ...{ [event.target.name]: event.target.value },
    });
  };
  return (
    <div>
      <FormControl component="fieldset" fullWidth style={{ marginTop: 20 }}>
        <FormGroup aria-label="position" row>
          <OutlinedTextField
            style={{ marginTop: 15 }}
            id="editableCommissionName"
            label="Название"
            InputLabelProps={{
              shrink: true,
            }}
            autoComplete="off"
            value={editableCommission.name}
            variant="outlined"
            type="text"
            name="name"
            fullWidth
            onChange={changeEditableCommisionHandler}
          />
          <OutlinedTextField
            style={{ marginTop: 15 }}
            id="editableCommissionDesc"
            label="Описание"
            multiline
            InputLabelProps={{
              shrink: true,
            }}
            autoComplete="off"
            value={editableCommission.description}
            variant="outlined"
            type="text"
            name="description"
            fullWidth
            onChange={changeEditableCommisionHandler}
          />
          <CommissionParticipants
            participants={commission.participants || []}
            handler={changeEditableCommissionParticipants}
          />
          <CommissionCategories
            participants={commission.categories || []}
            handler={changeEditableCommissionCategories}
          />
          <Button
            disabled={isLoading}
            onClick={saveCommission}
            variant="contained"
            color="primary"
            style={{ marginTop: 15 }}
          >
            <Typography variant="caption">Сохранить</Typography>
          </Button>
        </FormGroup>
      </FormControl>
    </div>
  );
};

export default EditCommission;
