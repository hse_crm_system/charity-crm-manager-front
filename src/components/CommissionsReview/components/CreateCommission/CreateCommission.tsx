import { Button, FormControl, FormGroup } from "@material-ui/core";
import { Typography } from "@material-ui/core";
import { useSnackbar } from "notistack";
import React, { ChangeEvent } from "react";
import { useState } from "react";
import { groupsApi } from "../../../../api/groupsApi";
import { OutlinedTextField } from "../../../../styledComponents/OutlinedTextField";
import { isFunction } from "lodash";
import CategoryFinder from "../../../CategoryFinder/CategoryFinder";
import { ICategoryPreViewBody } from "../../../CategoryFinder/Models";
interface IProps {
  trigger?: () => void;
}
const CreateCommission = ({ trigger }: IProps) => {
  const [selectedCategories, setSelectedCategories] = useState<
    ICategoryPreViewBody[]
  >([]);
  const [isPickingCategories, setIsPickingCategories] = useState(false);
  const [newCommissionName, setNewCommissionName] = useState<string>("");
  const [newDescription, setNewDescription] = useState<string>("");
  const [isLoading, setIsLoading] = useState(false);
  const changeNewCommissionNameHandler = (
    event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setNewCommissionName(event.target.value);
  };
  const { enqueueSnackbar } = useSnackbar();

  const CreateCommissionHandler = async () => {
    if (!newCommissionName.length) {
      enqueueSnackbar("Название фонда не может быть пустым", {
        variant: "error",
      });
      return;
    }
    if (!selectedCategories.length) {
      enqueueSnackbar(
        "Фонд обязан работать хотя бы с одной категорией заявок",
        {
          variant: "error",
        }
      );
      return;
    }

    try {
      setIsLoading(true);
      const isSeccess = await groupsApi.createGroup(
        newCommissionName,
        newDescription,
        selectedCategories
      );
      if (!isSeccess) {
        enqueueSnackbar("Ошибка создания фонда", {
          variant: "error",
        });
        return;
      }
      enqueueSnackbar("Фонд создан", {
        variant: "success",
      });
      setIsPickingCategories(false);
      isFunction(trigger) && trigger();
    } catch {
      enqueueSnackbar("Что-то пошло не так", {
        variant: "error",
      });
    } finally {
      setIsLoading(false);
    }
  };
  return (
    <div>
      <Typography variant="h6">Создание нового фонда</Typography>
      <FormControl component="fieldset" fullWidth style={{ marginTop: 10 }}>
        <FormGroup aria-label="position" row>
          <OutlinedTextField
            style={{ marginBottom: 20 }}
            id="createCommission"
            label="Название"
            InputLabelProps={{
              shrink: true,
            }}
            autoComplete="off"
            value={newCommissionName}
            variant="outlined"
            type="text"
            name="name"
            fullWidth
            onChange={changeNewCommissionNameHandler}
          />
          <OutlinedTextField
            style={{ marginBottom: 20 }}
            multiline
            id="createCommission"
            label="Описание"
            InputLabelProps={{
              shrink: true,
            }}
            autoComplete="off"
            value={newDescription}
            variant="outlined"
            type="text"
            name="name"
            fullWidth
            onChange={(
              event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
            ) => {
              setNewDescription(event.target.value);
            }}
          />
          {isPickingCategories ? (
            <CategoryFinder
              getter={setSelectedCategories}
              defaultSelectedCommissions={[]}
              isRatePick
            />
          ) : (
            <Button
              onClick={() => {
                setIsPickingCategories(true);
              }}
              fullWidth
              style={{
                marginBottom: 30,
              }}
              color="primary"
            >
              Выбрать категории
            </Button>
          )}
          <Button
            onClick={CreateCommissionHandler}
            variant="contained"
            color="primary"
            disabled={isLoading}
          >
            <Typography variant="caption">Создать</Typography>
          </Button>
        </FormGroup>
      </FormControl>
    </div>
  );
};

export default CreateCommission;
