import { Container, Paper } from "@material-ui/core";
import React from "react";
import { isMobile } from "../../const/Size";
import CreateCommission from "./components/CreateCommission/CreateCommission";
import ManageCommissions from "./components/ManageCommissions/ManageCommissions";

const CommissionsReview = () => {
  const [keyControll, setKeyControll] = React.useState(false);
  const remountManageComponent = () => {
    setKeyControll(!keyControll);
  };
  return (
    <Container maxWidth="md">
      <Paper style={{ textAlign: "left", padding: isMobile ? 10 : 30 }}>
        <CreateCommission trigger={remountManageComponent} />
      </Paper>
      <Paper style={{ textAlign: "left", padding: isMobile ? 10 : 30, marginTop: 40 }}>
      <ManageCommissions key={+keyControll} />
      </Paper>
    </Container>
  );
};

export default CommissionsReview;
