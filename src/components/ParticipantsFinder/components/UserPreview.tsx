import { LinearProgress, makeStyles, Paper } from "@material-ui/core";
import { IUserDisplay, UserHandlerType } from "../../AccountInfo/Models";
import UserPreviewBody from "./UserPreviewBody";

const stylesUserPreview = makeStyles(() => ({
  wrapper: {
    // backgroundColor: "rgba(255,255,255,0.8)",
    marginTop: 20,
    marginBottom: 20,
    width: "100%",
    zIndex: 2,
  },
}));

const UserPreview = ({
  data,
  handler,
  isLoading,
}: {
  data: IUserDisplay[];
  handler: UserHandlerType; // Тут снова плохо.
  isLoading: boolean;
}) => {
  const classes = stylesUserPreview();

  return (
    <Paper className={classes.wrapper} elevation={0}>
      {isLoading && <LinearProgress />}
      {data.slice(0, 120).map((userData) => (
        <UserPreviewBody {...userData} handler={handler} key={userData.id} />
      ))}
    </Paper>
  );
};

export default UserPreview;
