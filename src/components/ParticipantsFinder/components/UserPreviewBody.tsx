import { Typography } from "@material-ui/core";
import React from "react";
import { isMobile } from "../../../const/Size";
import { palette } from "../../../const/style/palette";
import { IUserDisplay } from "../../AccountInfo/Models";

const UserPreviewBody = ({
  handler = () => {},
  isSelected,
  ...oneUser
}: IUserDisplay) => {
  return (
    <div
      style={{
        justifyContent: "space-between",
        display: "flex",
        // width: "100%",
        padding: 10,
        // border: isSelected ? "1px solid #fff" : undefined,
        backgroundImage: isSelected ? palette.primaryGradient : undefined,
        margin: 5,
        borderRadius: 3,
        borderBottom: "1px solid #ccc",
      }}
      onClick={() => {
        handler(oneUser);
      }}
    >
      <Typography
        variant={isMobile ? "subtitle1" : "body2"}
        style={{ textAlign: "left" }}
        color={isSelected ? "textPrimary" : "initial"}
      >
        {`@${oneUser.username} - ${oneUser.firstName || ""} ${
          oneUser.lastName || ""
        }`.slice(0, 60)}
      </Typography>

      <Typography
        variant={isMobile ? "subtitle1" : "inherit"}
        style={{ textAlign: "right" }}
        color={isSelected ? "textPrimary" : "initial"}
      >
        id: #{oneUser.id}
      </Typography>
    </div>
  );
};

export default UserPreviewBody;
