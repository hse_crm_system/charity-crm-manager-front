import {
  debounce,
  FormControl,
  FormGroup,
} from "@material-ui/core";
import { useSnackbar } from "notistack";
import React, { ChangeEvent, useEffect, useState } from "react";
import { userApi } from "../../api/userApi";
import { OutlinedTextField } from "../../styledComponents/OutlinedTextField";
import { IUser, IUserDisplay } from "../AccountInfo/Models";
import UserPreview from "./components/UserPreview";

const ParticipantsFinder = ({
  getter,
  commissionParticipants,
}: {
  getter: (data: IUser[]) => void;
  commissionParticipants: IUser[];
}) => {
  const [isLoading, setIsLoading] = useState(false);
  const [searchedUsers, setSearchedUsers] = useState<IUser[]>([]);
  const [selectedUsers, setSelectedUsers] = useState<IUser[]>(
    commissionParticipants
  );
  const [usersListDisplay, setUsersListDisplay] = useState<IUserDisplay[]>([]);

  const [userFinder, setUserFinder] = useState<string>("");

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const fetchUsersList = React.useCallback(
    debounce(async (payload: string) => {
      setSearchedUsers((await fetchUsers(payload)) || []);
    }, 500),
    []
  );

  const selectUserHandler = (commission: IUser) => {
    selectedUsers.map((x) => x.id).indexOf(commission.id) === -1
      ? setSelectedUsers([...selectedUsers, commission])
      : setSelectedUsers(
          selectedUsers.slice().filter((x) => x.id !== commission.id)
        );
  };
  const { enqueueSnackbar } = useSnackbar();

  const fetchUsers = async (payload: string): Promise<IUser[] | undefined> => {
    try {
      setIsLoading(true);
      const resp = await userApi.fetchUsers(0, 100, payload);
      return resp;
    } catch (e: any) {
      console.log(e);
      enqueueSnackbar("Ошибка поиска", {
        variant: "error",
      });
    } finally {
      setIsLoading(false);
    }
  };
  const changeFindCommissionInputHandler = (
    event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setUserFinder(event.target.value);
    fetchUsersList(event.target.value);
  };
  useEffect(() => {
    getter(selectedUsers);
  }, [getter, selectedUsers]);
  useEffect(() => {
    setUsersListDisplay(
      [
        ...selectedUsers,
        ...searchedUsers
          .slice()
          .filter((x) => !selectedUsers.map((y) => y.id).includes(x.id)),
      ].map((x) => {
        return {
          ...x,
          isSelected: selectedUsers.indexOf(x) !== -1,
        };
      }) as IUserDisplay[]
    );
  }, [searchedUsers, selectedUsers]);
  useEffect(() => {
    fetchUsersList("");
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <div style={{ width: "100%" }}>
      <FormControl component="fieldset" fullWidth style={{ marginTop: 10 }}>
        <FormGroup aria-label="position" row>
          <OutlinedTextField
            // className={classes.newVoteField}
            label="Username"
            InputLabelProps={{
              shrink: true,
            }}
            autoComplete="off"
            value={userFinder}
            variant="outlined"
            type="text"
            name="username"
            fullWidth
            onChange={changeFindCommissionInputHandler}
          />
          <UserPreview
            {...{
              data: usersListDisplay,
              handler: selectUserHandler,
              isLoading
            }}
          />
        </FormGroup>
      </FormControl>
    </div>
  );
};

export default ParticipantsFinder;
