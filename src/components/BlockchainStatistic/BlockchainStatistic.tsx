import React, { useEffect, useState } from "react";
import { useLocation } from "react-router";
import queryString from "query-string";
import axios from "axios";
import { apiUrl } from "../../api/consts";
import { Box, Container, Dialog, Paper, Typography } from "@material-ui/core";

const BlockchainStatistic = () => {
  // Application
  // /crm/blockchain?applicationId=29&structure=Application&isAll=mysecretshowall

  // Transactions:
  // All donations (for Admin):
  // crm/blockchain?applicationId=28&structure=Transactions&isAll=mysecretshowall&access={accessToken}
  // My donations (with detaild):
  // /crm/blockchain?applicationId=31&structure=Transactions&isAll=mysecretshowall&isMine=1&access={accessToken}
  // My donations (without detaild):
  // /crm/blockchain?applicationId=31&structure=Transactions&isMine=1&access={accessToken}
  const [previewList, setPreviewList] = useState<any[]>([]);
  const location2 = useLocation();
  const params = queryString.parse(location2.search);
  const structure = params.structure;
  const isAllData = params.isAll === "mysecretshowall";
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const [dialogContent, setDialogContent] = useState("");
  const closeHandler = () => {
    setIsDialogOpen(!isDialogOpen);
  };
  const showAll = (val: any) => {
    if (!isAllData) {
      return;
    }
    setDialogContent(JSON.stringify(val, null, 4));
    closeHandler();
  };
  useEffect(() => {
    if (structure === "Application") {
      (async () => {
        try {
          const resp = await axios.get(
            `${apiUrl}/Blockchain/application/getHistory`,
            {
              params: {
                applicationId: params.applicationId,
              },
            }
          );
          setPreviewList(Object.values(resp.data).flat() as any[]);
        } catch {}
      })();
    }
    if (structure === "Transactions") {
      (async () => {
        try {
          const resp = await axios.get(
            params.isMine === "1" ? `${apiUrl}/Transaction/get_donation_flow_application` :`${apiUrl}/Transaction/get_all_donation_flow_application`,
            {
              params: {
                applicationId: params.applicationId,
              },
              headers: {
                Authorization: `Bearer ${params.access}`,
              },
            }
          );
          console.error(resp.data);
          setPreviewList(Object.values(resp.data).flat() as any[]);
        } catch {}
      })();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const mapApplicationStatus = (status: any) => {
    switch (status) {
      case "0":
        return "Created";
      case "1":
        return "Declined Permanently";
      case "2":
        return "Needs specification";
      case "3":
        return "Waits for voting";
      case "5":
        return "Fundraising";
      case "6":
        return "Waits for implementation";
      case "7":
        return "Implemented";
      case "8":
        return "Canceled";
      case "9":
        return "Foundations Refused";
      default:
        return "Unknown status";
    }
  };
  return (
    <>
      <Dialog open={isDialogOpen} onClose={closeHandler}>
        <Box margin={2}>
          <Typography
            style={{
              whiteSpace: "pre",
              fontFamily: "monospace",
            }}
          >
            {dialogContent}
          </Typography>
        </Box>
      </Dialog>
      <Container maxWidth="md">
        <Box textAlign="left">
          <Typography variant="h5">{structure}</Typography>
          {previewList.map((x: any) => {
            return (
              <Box textAlign="left" borderBottom="1px solid #ccc" marginY={2}>
                <Paper
                  style={{
                    cursor: isAllData ? "pointer" : "initial",
                  }}
                  onClick={() => {
                    showAll(x);
                  }}
                >
                  <Box marginY={1} marginX={1}>
                    <Typography>
                      {new Date(Date.parse(x.timestamp)).toLocaleString()}
                    </Typography>
                    <Typography>
                      {x?.data?.application_status
                        ? `Application status: ${mapApplicationStatus(
                            x?.data?.application_status
                          )}`
                        : ""}
                    </Typography>
                    <Typography>
                      {x?.data?.value
                        ? `Donated value: +${x?.data?.value}`
                        : ""}
                    </Typography>
                    <Typography>
                      {x.txid ? `TxID: ${x.txid}` : "No tx id"}
                    </Typography>
                  </Box>
                </Paper>
                {x.block && (
                  <Box marginX={1}>
                    <Typography variant="overline">Block info</Typography>
                    <Typography variant="body2">
                      {x.block.current_hash
                        ? `Current hash: ${x.block.current_hash}`
                        : ""}
                    </Typography>
                    <Typography variant="body2">
                      {x.block.data_hash
                        ? `Data hash: ${x.block.data_hash}`
                        : ""}
                    </Typography>
                    <Typography variant="body2">
                      {x.block.number ? `Number: ${x.block.number}` : ""}
                    </Typography>
                    <Typography variant="body2">
                      {x.block.previous_hash
                        ? `Previous hash: ${x.block.previous_hash}`
                        : ""}
                    </Typography>
                  </Box>
                )}
              </Box>
            );
          })}
        </Box>
      </Container>
    </>
  );
};

export default BlockchainStatistic;
