import { apiUrl } from "./consts";
import axios from "axios";
import {
  IChatMessage,
  IFile,
  IVoteBlockProps,
  VoteResult,
  VoteStatus,
} from "../components/VoteBlock/state";
import { max } from "lodash";

export const applicationsApi = {
  searchCategories: (term: string) => {
    return axios
      .put(`${apiUrl}/Category/search?SearchTerm=${term}&offset=0&limit=10`)
      .then(({ data }) => data.categoryList);
  },
  getCategories: () => {
    return axios
      .get(`${apiUrl}/Category/get_all`, { params: { offset: 0, limit: 40 } })
      .then(({ data }) => data.categoryList);
  },
  vote: (votingId: string, choice: VoteResult, userId?: string) => {
    return axios
      .post(`${apiUrl}Vote/vote`, {
        votingId,
        userId,
        choice,
      })
      .then((resp) => resp.status === 200)
      .catch(() => false);
  },
  sendReport: ({
    title,
    description,
    media,
    applicationId,
  }: {
    title: string;
    description: string;
    media: IFile[];
    applicationId: string;
  }): Promise<boolean | IFile[]> =>
    axios
      .post(`${apiUrl}/Report/create`, {
        applicationId,
        title,
        description,
        temporaryUploadedFiles: {
          files: media,
        },
      })
      .then((resp) => resp.data.files.files)
      .catch(() => false),
  chatSendMessage: ({
    text,
    media,
    applicationId,
  }: {
    text: string;
    media: IFile[];
    applicationId: string;
  }): Promise<boolean | IFile[]> =>
    axios
      .post(`${apiUrl}/Chat/send`, {
        applicationId,
        message: text,
        tempMedia: {
          files: media,
        },
      })
      .then((resp) => resp.data.files.files)
      .catch(() => false),
  chatFetch: (applicationId: string): Promise<IChatMessage[]> =>
    axios
      .post(`${apiUrl}/Chat/get_application_chat`, {
        applicationId,
        offset: 0,
        limit: 1000,
      })
      .then(({ data }) => {
        return data?.messages;
      }),
  fetchVotingById: async (
    id: string,
    userId?: string
  ): Promise<IVoteBlockProps> =>
    axios
      .get(`${apiUrl}/Application/get_id?request=${id}`)
      .then(async ({ data }) => {
        const repDat = await axios.get(
          `${apiUrl}/Report/getApplicationReports`,
          {
            params: {
              applicationId: id,
              offset: 0,
              limit: 200,
            },
          }
        );
        const vote = data;
        // const initiator = vote.initiator;
        // const commissions = vote.participatingGroups;
        const votesSum =
          vote.results?.for + vote.results?.against + vote.results?.abstained ||
          1;
        // const isMyVotesOnModeration = userId === initiator.id;
        return {
          ...vote,
          blockchainEnabled: vote.blockchainEnabled || false,
          description: vote.description,
          id: vote.id,
          // createdBy: {
          //   ...initiator,
          //   username: initiator.userName,
          // },
          dateStart:
            max([new Date(vote?.approvalTime), new Date(vote?.creationTime)]) ||
            new Date(vote?.approvalTime),
          dateEnd: new Date(vote.expectedGatheringEnd),
          headline: vote.title,
          myVote: vote?.myVote?.choice,
          categoryName: vote?.categoryName,
          raisedSum: vote.alreadyRaised,
          expectedSum: vote.expectedSum,
          foundationName: vote.foundationName,
          reports: repDat.data.reports,
          status: (() => {
            // if (
            //   !isMyApplications
            //   // &&
            //   // vote.votingStatus === VoteStatus.IN_PROGRESS
            // ) {
            //   return VoteStatus.CREATED;
            // }
            // if (
            //   isMyVotesOnModeration &&
            //   vote.votingStatus === VoteStatus.ON_MODERATION
            // ) {
            //   return VoteStatus.MY_VOTE_ON_MODERATION;
            // }
            // if (vote?.myVote && vote.votingStatus === VoteStatus.IN_PROGRESS) {
            //   return VoteStatus.VOTED;
            // }
            // return VoteStatus.IMPLEMENTED;
            return vote.applicationStatus;
          })(),
          resultsProcents: {
            acceptProcent: (vote.results?.for / votesSum).toFixed(),
            rejectProcent: (vote.results?.against / votesSum).toFixed(),
            skipProcent: (vote.results?.abstained / votesSum).toFixed(),
          },
          // commissions: commissions?.map((com: any) => ({
          //   name: com.groupName,
          //   id: com.id,
          // })),
          results: (() => {
            if (
              vote.results?.abstained > vote.results?.against &&
              vote.results?.abstained > vote.results?.for
            ) {
              return VoteResult.SKIP;
            }
            if (
              vote.results?.against > vote.results?.abstained &&
              vote.results?.against > vote.results?.for
            ) {
              return VoteResult.REJECT;
            }
            if (
              vote.results?.for > vote.results?.against &&
              vote.results?.for > vote.results?.abstained
            ) {
              return VoteResult.ACCEPT;
            }
          })(),
        };
      }),
  pickApplication: (applicationId: string): Promise<boolean> => {
    return axios
      .post(
        `${apiUrl}/ApplicationManagement/pick_unassigned?applicationId=${applicationId}`
      )
      .then((resp) => resp.status === 200)
      .catch(() => false);
  },
  declineVoting: (
    applicationId?: string,
    comment?: string
  ): Promise<boolean> => {
    return axios
      .post(
        `${apiUrl}/ApplicationManagement/decline_unassigned?applicationId=${applicationId}`,
        { comment }
      )
      .then((resp) => resp.status === 200)
      .catch(() => false);
  },
  cancelVoting: (votingId?: string, userId?: string): Promise<boolean> => {
    return axios
      .put(`${apiUrl}VotingStatus/cancel`, {
        votingId,
        updateInitiatorUserId: userId,
      })
      .then((resp) => resp.status === 200)
      .catch(() => false);
  },

  fetchVotings: (
    offset: number,
    limit: number,
    states: VoteStatus[],
    {
      userId,
      isMyVotesOnModeration,
      ascending,
      field,
      isCrmManager,
      isMyApplications,
      categoryId,
    }: {
      userId?: string;
      isMyVotesOnModeration?: boolean;
      ascending?: boolean;
      field?: number;
      isCrmManager?: boolean;
      categoryId?: string;
      isMyApplications?: boolean;
    }
  ): Promise<IVoteBlockProps[]> =>
    axios
      .post(
        isCrmManager
          ? `${apiUrl}/ApplicationManagement/get_unassigned` // Здесь получение для админа
          : !isMyApplications
          ? `${apiUrl}/ApplicationManagement/get_unassigned`
          : `${apiUrl}/ApplicationManagement/get_my_applications`,
        {
          // desiredStatus: states,
          // initiatorId: userId ? [userId] : undefined,
          offset: offset,
          limit: limit,
          categoryId: categoryId,
          desiredCategoryId: categoryId,
          // sorting: {
          //   ascending,
          //   field,
          // },
        }
      )
      .then(({ data }) => {
        const backVotings = Object.values(data)[0] as any;
        console.log(Object.values(data));
        const resVotes = backVotings?.map((vote: any): IVoteBlockProps => {
          // const initiator = vote.initiator;
          const commissions = vote.participatingGroups;
          const votesSum =
            (vote.results?.for +
              vote.results?.against +
              vote.results?.abstained || 1) / 100;

          return {
            blockchainEnabled: vote.blockchainEnabled,
            description: vote.description,
            id: vote.id,

            // createdBy: {
            //   ...initiator,
            //   username: initiator.userName,
            // },
            // dateStart:
            //   max([
            //     new Date(vote?.approvalTime),
            //     new Date(vote?.creationTime),
            //   ]) || new Date(vote?.approvalTime),
            dateEnd: new Date(vote.expectedGatheringEnd),
            headline: vote.title,
            categoryName: vote.categoryName,
            expectedSum: vote.expectedSum,
            raisedSum: vote.alreadyRaised,
            foundationName: vote.foundationName,
            status: (() => {
              return vote.applicationStatus;
              // if (isCrmManager) {
              //   return VoteStatus.SHOW_FOR_CRM_MANAGER;
              // }
              if (
                !isMyApplications
                // &&
                // vote.votingStatus === VoteStatus.IN_PROGRESS
              ) {
                return VoteStatus.CREATED;
              }
              // if (
              //   isMyVotesOnModeration &&
              //   vote.votingStatus === VoteStatus.ON_MODERATION
              // ) {
              //   return VoteStatus.MY_VOTE_ON_MODERATION;
              // }
              // if (
              //   vote?.myVote &&
              //   vote.votingStatus === VoteStatus.IN_PROGRESS
              // ) {
              //   return VoteStatus.VOTED;
              // }
              return VoteStatus.IMPLEMENTED;
            })(),
            // resultsProcents: {
            //   acceptProcent: (vote.results?.for / votesSum).toFixed(),
            //   rejectProcent: (vote.results?.against / votesSum).toFixed(),
            //   skipProcent: (vote.results?.abstained / votesSum).toFixed(),
            // },
            // commissions: commissions?.map((com: any) => ({
            //   name: com.groupName,
            //   id: com.id,
            //   description: com?.groupDescription,
            // })),
            // myVote: vote?.myVote?.choice,
            // results: (() => {
            //   if (
            //     vote.results?.abstained > vote.results?.against &&
            //     vote.results?.abstained > vote.results?.for
            //   ) {
            //     return VoteResult.SKIP;
            //   }
            //   if (
            //     vote.results?.against > vote.results?.abstained &&
            //     vote.results?.against > vote.results?.for
            //   ) {
            //     return VoteResult.REJECT;
            //   }
            //   if (
            //     vote.results?.for > vote.results?.against &&
            //     vote.results?.for > vote.results?.abstained
            //   ) {
            //     return VoteResult.ACCEPT;
            //   }
            // })(),
          };
        });
        return resVotes;
      }),

  fetchAdminVotings: (
    offset: number,
    limit: number,
    states: VoteStatus[],
    {
      userId,
      isMyVotesOnModeration,
      ascending,
      field,
      isMyApplications,
      categoryId,
      searchTerm,
    }: {
      userId?: string;
      isMyVotesOnModeration?: boolean;
      ascending?: boolean;
      field?: number;
      categoryId?: string;
      isMyApplications?: boolean;
      searchTerm?: string;
    }
  ): Promise<IVoteBlockProps[]> =>
    axios
      .post(
        `${apiUrl}/ApplicationManagement/admin/get_applications`, // Здесь получение для админа

        {
          // desiredStatus: states,
          // initiatorId: userId ? [userId] : undefined,
          offset: offset,
          limit: limit,
          categoryId: categoryId,
          desiredCategoryId: categoryId,
          searchTerm,
          // sorting: {
          //   ascending,
          //   field,
          // },
        }
      )
      .then(({ data }) => {
        const backVotings = Object.values(data)[0] as any;
        // console.log(Object.values(data));
        const resVotes = backVotings?.map((vote: any): IVoteBlockProps => {
          // const initiator = vote.initiator;
          const commissions = vote.participatingGroups;
          const votesSum =
            (vote.results?.for +
              vote.results?.against +
              vote.results?.abstained || 1) / 100;

          return {
            blockchainEnabled: vote.blockchainEnabled,
            description: vote.description,
            id: vote.id,

            // createdBy: {
            //   ...initiator,
            //   username: initiator.userName,
            // },
            // dateStart:
            //   max([
            //     new Date(vote?.approvalTime),
            //     new Date(vote?.creationTime),
            //   ]) || new Date(vote?.approvalTime),
            dateEnd: new Date(vote.expectedGatheringEnd),
            headline: vote.title,
            categoryName: vote.categoryName,
            expectedSum: vote.expectedSum,
            raisedSum: vote.alreadyRaised,
            foundationName: vote.foundationName,
            status: (() => {
              return vote.applicationStatus;
              if (
                !isMyApplications
                // &&
                // vote.votingStatus === VoteStatus.IN_PROGRESS
              ) {
                return VoteStatus.CREATED;
              }
              // if (
              //   isMyVotesOnModeration &&
              //   vote.votingStatus === VoteStatus.ON_MODERATION
              // ) {
              //   return VoteStatus.MY_VOTE_ON_MODERATION;
              // }
              // if (
              //   vote?.myVote &&
              //   vote.votingStatus === VoteStatus.IN_PROGRESS
              // ) {
              //   return VoteStatus.VOTED;
              // }
              return VoteStatus.IMPLEMENTED;
            })(),
            // resultsProcents: {
            //   acceptProcent: (vote.results?.for / votesSum).toFixed(),
            //   rejectProcent: (vote.results?.against / votesSum).toFixed(),
            //   skipProcent: (vote.results?.abstained / votesSum).toFixed(),
            // },
            // commissions: commissions?.map((com: any) => ({
            //   name: com.groupName,
            //   id: com.id,
            //   description: com?.groupDescription,
            // })),
            // myVote: vote?.myVote?.choice,
            // results: (() => {
            //   if (
            //     vote.results?.abstained > vote.results?.against &&
            //     vote.results?.abstained > vote.results?.for
            //   ) {
            //     return VoteResult.SKIP;
            //   }
            //   if (
            //     vote.results?.against > vote.results?.abstained &&
            //     vote.results?.against > vote.results?.for
            //   ) {
            //     return VoteResult.REJECT;
            //   }
            //   if (
            //     vote.results?.for > vote.results?.against &&
            //     vote.results?.for > vote.results?.abstained
            //   ) {
            //     return VoteResult.ACCEPT;
            //   }
            // })(),
          };
        });
        return resVotes;
      }),
};
