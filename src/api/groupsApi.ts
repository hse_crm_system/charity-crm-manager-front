import { ICategory } from "./../components/CategoryFinder/Models";
import axios from "axios";
import { ICommission } from "../components/CommissionsFinder/Models";
import { apiUrl } from "./consts";

export const groupsApi = {
  fetchFullGroup: async (id: string): Promise<ICommission> => {
    const participants = await (
      await axios.post(`${apiUrl}/Foundation/get_foundation_managers`, {
        foundationId: id,
        offset: 0,
        limit: 400,
      })
    ).data.managers.map((x: any) => ({
      id: x.id,
      username: x.username,
      firstName: x.fullName,
      // lastName: string;
    }));
    return axios
      .get(`${apiUrl}/Foundation/get_id?request=${id}`)
      .then(({ data }) => {
        const group = data;
        return {
          id: group.id,
          name: group.name,
          description: group.description,
          participants,
          categories: group.categories.map((x: any) => ({
            name: x.categoryName,
            id: x.categoryId,
            categoryRate: x.categoryRate,
            isSelected: true,
          })),
          // participants: group.experts?.map((expert: any) => ({
          //   id: expert.id,
          //   username: expert.userName,
          //   firstName: expert.firstName,
          //   lastName: expert.lastName,
          // })),
        };
      });
  },
  updateGroup: (group: ICommission): Promise<boolean> => {
    return axios
      .post(`${apiUrl}/Foundation/update`, {
        id: group.id,
        newName: group.name,
        newDescription: group.description,
        newActiveStatus: true,
        categoriesList: group.categories?.map((x) => ({
          categoryId: x.id,
          categoryRate: x.categoryRate,
        })),
      })
      .then(
        (resp) => resp.status === 200
        //    {
        //     if (resp.status !== 200) {
        //         return false;
        //     }
        //     return axios.put
        //   }
      )
      .then(() => {
        return groupsApi
          .addUserToFund(
            group.id,
            group.participants?.map((user) => user.id) || []
          )
          .then(() => true);
      })
      .catch(() => false);
  },
  createGroup: (
    name: string,
    newDescription: string,
    categories: ICategory[]
  ): Promise<boolean> => {
    return axios
      .post(`${apiUrl}/Foundation/create`, {
        name: name,
        description: newDescription,
        isActive: true,
        categoriesList: categories.map((x) => ({
          categoryId: x.id,
          categoryRate: x.categoryRate,
        })),
      })
      .then((resp) => resp.status === 200)
      .catch(() => false);
  },
  addUserToFund: (fundId: string, userIds: string[]) => {
    return axios.post(`${apiUrl}/Foundation/assign_managers`, {
      foundationId: fundId,
      managerIds: userIds,
    });
  },
  fetchGroups: (
    offset: number,
    limit: number,
    name?: string
  ): Promise<ICommission[]> => {
    return axios
      .put(`${apiUrl}/Foundation/search`, {
        searchTerm: name,
        offset: offset,
        limit: limit,
      })
      .then(({ data }) => {
        const backCommissions = data.foundations;
        return backCommissions?.map((group: any) => {
          return {
            id: group.id?.toString(),
            name: group.name,
            description: group.description,
            participants: [],
          };
        });
      });
  },
};
