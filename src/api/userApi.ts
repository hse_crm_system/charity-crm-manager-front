import axios from "axios";
import { IUser } from "../components/AccountInfo/Models";
import { IBeneficiar } from "../components/BeneficiaryReview/models";
import {
  EUserRoles,
  ILoginForm,
  ITokens,
} from "../components/LoginScreen/Models";
import { IFile } from "../components/VoteBlock/state";
import { apiUrl } from "./consts";

export const userApi = {
  login: (
    data: ILoginForm
  ): Promise<{ access_token: string; refresh_token: string }> => {
    // const params = new URLSearchParams();
    // params.append("password", data.password);
    // params.append("username", data.email);
    // params.append("client_id", "VotingApi");
    // params.append("grant_type", "password");
    return (
      axios
        .post(`${apiUrl}/User/auth`, {
          password: data.password,
          userName: data.email,
          grantType: "password",
        })
        // .post("http://sts.crmapix.xyz/connect/token", params, {
        //   headers: { "Content-Type": "application/x-www-form-urlencoded" },
        // })
        .then((resp) => ({
          access_token: resp.data.accessToken,
          refresh_token: resp.data.refreshToken,
        }))
    );
  },
  deleteToken: (refreshToken: string) => {
    console.log(refreshToken);
  },
  refreshTokens: (refreshToken: string, username: string): Promise<ITokens> => {
    // const params = new URLSearchParams();
    // params.append("client_id", "VotingApi");
    // params.append("grant_type", "refresh_token");
    // params.append("refresh_token", refreshToken);

    return axios
      .post(`${apiUrl}/User/auth`, {
        userName: username,
        refreshToken: refreshToken,
        grantType: "refresh_token",
      })
      .then((resp) => ({
        accessToken: resp.data.accessToken,
        refreshToken: resp.data.refreshToken,
      }));
  },
  approveBeneficiarApplication: (userId: string): Promise<boolean> =>
    axios
      .post(`${apiUrl}/Beneficiary/approve_application`, {
        userId,
      })
      .then(({ status }) => status === 200)
      .catch(() => false),
  declineBeneficiarApplication: (userId: string): Promise<boolean> =>
    axios
      .post(`${apiUrl}/Beneficiary/decline_application`, {
        userId,
      })
      .then(({ status }) => status === 200)
      .catch(() => false),
  fetchbeneficiarApplications: (
    offset: number,
    limit: number
    // filter?: string
  ): Promise<IBeneficiar[]> =>
    axios
      .put(`${apiUrl}/Beneficiary/get_all`, {
        offset,
        limit,
      })
      .then(({ data }) => data.beneficiaryApplications),
  fetchUsers: (
    offset: number,
    limit: number,
    filter?: string
  ): Promise<IUser[]> =>
    axios
      .get(`${apiUrl}/User/search`, {
        params: {
          SearchTerm: filter || "",
          Offset: offset,
          Limit: limit,
        },
      })
      .then(({ data }) => {
        const usersBack = data.userList;
        return usersBack?.map((user: any) => {
          return {
            id: user.id,
            username: user.userName,
            firstName: user.fullName,
          };
        });
      }),
  uploadFiles: (files: File[]): Promise<IFile[]> => {
    var formData = new FormData();
    files.forEach((x) => formData.append("uploadedFiles", x, x.name));
    return axios
      .post(`${apiUrl}/FileLoader/load_files_temp`, formData, {
        headers: { "Content-Type": "multipart/form-data" },
      })
      .then(({ data }) => data.files);
  },
};
