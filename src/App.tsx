import { makeStyles } from "@material-ui/core";
import axios from "axios";
import React, { useContext } from "react";
import { parseJwt } from "./api/tools/parseJWT";
import { userApi } from "./api/userApi";
import { ELoadingState, EUserRoles } from "./components/LoginScreen/Models";
import { UserContext } from "./components/LoginScreen/UserStore";
import { MenuButtonEnum } from "./components/Menu/Enums";
import Menu from "./components/Menu/Menu";
import { IButtonsData, IMenuProps } from "./components/Menu/state";
import PageContent from "./components/PageContent/PageContent";
import { RedirectLinks } from "./const/RedirectLinks";
import "./style.css";

const AdminButtons: IButtonsData[] = [
  // {
  //   text: "Администрирование всех заявок",
  //   link: RedirectLinks.APPLICATIONS_REVIEW,
  //   type: MenuButtonEnum.VOTES_REVIEW,
  // },
];
const FundManagerButtons: IButtonsData[] = [
  {
    text: "Администрирование заявок",
    link: RedirectLinks.VOTES_PARTICIPATE_LIST,
    type: MenuButtonEnum.VOTES_PARTICIPATE_LIST,
  },
];
const CRMManagerButtons: IButtonsData[] = [
  {
    text: "Администрирование фондов",
    link: RedirectLinks.FUND_REVIEW,
    type: MenuButtonEnum.FUND_REVIEW,
  },
  {
    text: "Заявки на сбор средств",
    link: RedirectLinks.APPLICATIONS_REVIEW,
    type: MenuButtonEnum.APPLICATIONS_REVIEW,
  },
  {
    text: "Администрирование бенефициаров",
    link: RedirectLinks.BENEFICIARY_REVIEW,
    type: MenuButtonEnum.BENEFICIARY_REVIEW,
  },
];

export const stylesApp = makeStyles(() => ({
  App: {
    "&::after": {
      height: window.innerHeight,
    },
  },
}));
function App() {
  const classes = stylesApp();
  const { state, dispatch } = useContext(UserContext);
  const [leftMenuData, setLeftMenuData] = React.useState<IMenuProps>({
    buttonsDataList: [],
  });

  const fetchTokens = React.useCallback(() => {
    const rt = localStorage.getItem("refreshToken");
    if (rt) {
      (async () => {
        try {
          const updatedTokens = await userApi.refreshTokens(rt, parseJwt(localStorage.getItem("accessToken") || '').name || '');
          if (updatedTokens) {
            dispatch({ type: "SET_TOKENS", payload: updatedTokens });
            localStorage.setItem("accessToken", updatedTokens.accessToken);
            localStorage.setItem("refreshToken", updatedTokens.refreshToken);
            dispatch({
              type: "SET_USER_DATA",
              payload: parseJwt(updatedTokens.accessToken),
            });
            axios.defaults.headers.common[
              "Authorization"
            ] = `Bearer ${updatedTokens.accessToken}`;
          } else {
            throw new Error("Request error");
          }
        } catch {
          dispatch({
            type: "SET_LOADING_STATE",
            payload: ELoadingState.ERROR,
          });
        }
      })();
    } else {
      dispatch({
        type: "SET_LOADING_STATE",
        payload: ELoadingState.NEVER,
      });
    }
  }, [dispatch]);

  React.useEffect(() => {
    fetchTokens();
  }, [fetchTokens]);

  React.useEffect(() => {
    const refreshTokensInterval = setInterval(() => {
      fetchTokens();
    }, 3600 * 1000);
    return () => {
      clearInterval(refreshTokensInterval);
    };
  }, [fetchTokens]);

  React.useEffect(() => {
    if (state.LodingState === ELoadingState.LOADED) {
      let newButtonList: IButtonsData[] = [
        {
          text: "Выйти",
          link: RedirectLinks.LOGIN_SCREEN,
          handler: () => {
            (async () => {
              state?.data?.tokens?.refreshToken &&
                userApi.deleteToken(state?.data?.tokens?.refreshToken);
            })();
            dispatch({ type: "LOGOUT" });
          },
          type: MenuButtonEnum.LOGOUT,
        },
      ];
      if (state?.data?.userData?.role.includes(EUserRoles.CRM_ADMIN)) {
        newButtonList = [...AdminButtons, ...newButtonList];
      }
      if (state?.data?.userData?.role.includes(EUserRoles.CEM_MANAGER)) {
        newButtonList = [...CRMManagerButtons, ...newButtonList];
      }
      if (state?.data?.userData?.role.includes(EUserRoles.CRM_FUND_MANAGER)) {
        newButtonList = [...FundManagerButtons, ...newButtonList];
      }
      setLeftMenuData({ ...leftMenuData, buttonsDataList: newButtonList });
    } else {
      setLeftMenuData({ ...leftMenuData, buttonsDataList: [] });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [state?.data?.userData?.role]);
  return (
    <div
      className={"App " + classes.App}
      style={{
        minHeight: window.innerHeight,
      }}
    >
      {leftMenuData.buttonsDataList.length ? <Menu {...leftMenuData} /> : ""}
      <PageContent />
    </div>
  );
}

export default App;
