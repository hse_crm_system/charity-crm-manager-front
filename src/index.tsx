import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import { CssBaseline, ThemeProvider } from "@material-ui/core";
import { BrowserRouter } from "react-router-dom";
import { theme } from "./const/style/theme";
import { SnackbarProvider } from "notistack";
import { UserStore } from "./components/LoginScreen/UserStore";

ReactDOM.render(
  <React.StrictMode>
    <SnackbarProvider maxSnack={3}>
      <UserStore>
        <BrowserRouter basename={process.env.PUBLIC_URL}>
          <CssBaseline />
          <ThemeProvider theme={theme}>
            <App />
          </ThemeProvider>
        </BrowserRouter>
      </UserStore>
    </SnackbarProvider>
  </React.StrictMode>,
  document.getElementById("root")
);
