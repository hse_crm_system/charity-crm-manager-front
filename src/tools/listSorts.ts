export const listSorts = {
  sortToColumns: <T>(list: T[], compareBy: (i: T) => number): T[][] => {
    const a: T[][] = [[], []];

    const getSortedList = (list: T[]): number => {
      return list.map(compareBy).reduce((x, y) => x + y, 0);
    };

    list.forEach((x) => {
      a[+(getSortedList(a[0]) > getSortedList(a[1]))].push(x);
    });

    return a;
  },
};
