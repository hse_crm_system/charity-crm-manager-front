import { withStyles, Theme, createStyles, Badge } from "@material-ui/core";

export const StyledBadge = withStyles((theme: Theme) =>
  createStyles({
    badge: {
      right: "50%",
      top: -30,
      // border: `2px solid ${theme.palette.background.paper}`,
      padding: '0 4px',
    },
  }),
)(Badge);