import { Button, withStyles } from "@material-ui/core";

export const MenuButton = withStyles({
  root: {
    boxShadow: "none",
    textTransform: "none",
    fontSize: 16,
    padding: "6px 12px",
    position: "relative",
    lineHeight: 2.0,
    color: "#FFF"
    // "&::after": {
    //   content: `''`,
    //   position: "absolute",
    //   left: "5%",
    //   bottom: "0px",
    //   width: "90%",
    //   height: "2px",
    //   background: palette.secondaryBackground,
    //   BorderRight: 0,
    // },
  },
})(Button);
