import { withStyles, Theme, createStyles, InputBase } from "@material-ui/core";
import { palette } from "../const/style/palette";

export const HeadSelect = withStyles((theme: Theme) =>
  createStyles({
    root: {
      "label + &": {
        marginTop: theme.spacing(3),
      },
      color: "#FFF",
    },
    select:{
      backgroundColor: "#123"
    },
    input: {
      borderRadius: 4,
      position: "relative",
      // backgroundColor: theme.palette.background.paper,
      color: palette.text,
      fontSize: 14,
      padding: "10px 26px 10px 12px",
      transition: theme.transitions.create(["border-color", "box-shadow"]),
    },
  })
)(InputBase);
