import { TextField, withStyles } from "@material-ui/core";
import { palette } from "../const/style/palette";

export const OutlinedTextField = withStyles({
  root: {
    "& label": {
      color: palette.secondaryText,
    },
    "& label.Mui-focused": {
      color: palette.primary,
      transition: "1s",
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: palette.secondaryText,
      borderWidth: 2,
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        transition: "1s",
        borderColor: palette.secondaryText,
        borderWidth: 2,
        color: palette.text,
      },
      "&:hover fieldset": {
        borderColor: palette.primary,
        transition: "1s",
        borderWidth: 2,
      },

      "&.Mui-focused fieldset": {
        borderColor: palette.primary,
        transition: "1s",
        borderWidth: 2,
      },
    },
  },
})(TextField);
