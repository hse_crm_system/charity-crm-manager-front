export enum RedirectLinks {
  VOTES_PARTICIPATE_LIST = "/VotesParticipateList/",
  MY_VOTES_LIST = "/myVotes/",
  FUND_REVIEW = "/commissionsReview/",
  APPLICATIONS_REVIEW = "/votesReview/",
  BENEFICIARY_REVIEW = "/beneficiaryReview/",
  LOGIN_SCREEN = "/login/",
  ONE_VOTE = "/vote/",
  ADMIN_ONE_VOTE = "/adminVote/",
  BLOCKCHAIN_STATISTIC = "/blockchain/"
}
