const windowWidth = document.documentElement.clientWidth;
export const windowHeight = document.documentElement.clientHeight;
export const isMobile = !(windowWidth > 900);
export const isMd = !(windowWidth > 1150);

