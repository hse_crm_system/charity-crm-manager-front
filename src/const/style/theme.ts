import { createMuiTheme, Theme } from "@material-ui/core";
import { Shadows } from "@material-ui/core/styles/shadows";
import { palette } from "./palette";

export const theme: Theme = createMuiTheme({
  typography: {
    fontFamily: 'Montserrat',
},
  shadows: Array(25).fill("none") as Shadows,
  palette: {
    primary: {
      main: palette.primary,
      contrastText: "#fff",
    },
    secondary: {
      main: "#FFF",
    },
    background: {
      paper: "#FFF",
    },
    text: {
      secondary: "#444",
    },
    action: {
      disabled: "#777",
    },
  },
  overrides: {
    MuiButton: {
      root: {
        color: "#FFF",
      },
      outlined: {
        borderColor: "#222",
      },
    },
    MuiFab: {
      root: {
        boxShadow: "0px 7px 17px 0px rgb(0 0 0 / 10%)",
      },
    },
    MuiTypography: {
      subtitle2: {
        fontWeight: 500,
        fontSize: 16,
      },
      caption: {
        color: "#FFF",
      },
      colorTextPrimary: {
        color: "#FFF",
      },
      root: {
        color: "#000",
      },
      h5: {
        fontWeight: 500,
      },
      h4: {
        fontWeight: 500,
      },
    },
    MuiDrawer: {
      root: {
        backgroundColor: "#FFF",
      },
    },
    MuiPaper: {
      root: {
        backgroundColor: "rgba(255, 255, 255, 0.92)",
        border: "2px solid #fff",
        // boxShadow: "0px 4px 207px 163px rgba(29, 43, 94, 0.04) !important"
        // boxSizing: "border-box",
        // backgroundColor: "rgba(0, 0, 0, 0.22)",
        // backgroundColor: "rgba(0, 0, 0, 0.36)",
        // backdropFilter: "blur(8px)",
      },
    },
  },
});
