export const palette = {
  rejectVote: "#F26D66",
  acceptVote: "#68D8B0",
  skipVote: "#3FAEE6",
  
  primary: "#FC6E4A",
  // primary: "#ccc",
  primaryBackground: "#F2F5FF",
  text: "#222",
  secondaryText: "#777",
  background: "#FFF",
  secondaryBackground: "#E7E7E7", //E1E1E1
  contrastText: "#FFF",
  primaryGradient: "linear-gradient(86.02deg, #FC6E4A -5.53%, #F2947C 106.31%)"
};


//#FDFDFF